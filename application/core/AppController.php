<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppController extends CI_Controller {

	public function __construct() {
		parent::__construct();	
		if (!$company = $this->app_model->findCompanyBySlug($this->uri->segment(1))) {
			show_404();
		}
		$this->global_var->set('company', $company);		
		if (!isSignedIn()) {
			$this->redirect->with('errorMessage')->guest(getGlobalVar('company')->slug . '/signin');
		}
	}
}