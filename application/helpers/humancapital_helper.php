<?php
function labelRequestType($request_type) {
	$request_label = toSelect(lang('_request_type'), $request_type);
	switch ($request_type) {
		case 'add':
			return '<span class="label label-primary">'. $request_label .'</span>';
			break;
		case 'update':
			return '<span class="label label-warning">'. $request_label .'</span>';
			break;
		case 'remove':
			return '<span class="label label-danger">'. $request_label .'</span>';
			break;
		default:
			return '';
			break;
	}
}

function labelRequestStatus($status) {
	switch ($status) {
		case 'canceled':
			return '<label class="label label-warning">'.toSelect(lang('_request_status'), $status).'</label>';
			break;	
		case 'rejected':
			return '<label class="label label-danger">'.toSelect(lang('_request_status'), $status).'</label>';
			break;		
		case 'approved':
			return '<label class="label label-success">'.toSelect(lang('_request_status'), $status).'</label>';
			break;	
		case 'waiting':
			return '<label class="label label-primary">'.toSelect(lang('_request_status'), $status).'</label>';
			break;	
		default:
			return '';
			break;
	}				
}

function urlRequestDetail($data, $id) {
	$detail_url = '';	
	switch ($data) {
		case 'leave':
			$detail_url = getGlobalVar('company')->slug . '/leave/detail/' . $id;
			break;
		case 'permit':
			$detail_url = getGlobalVar('company')->slug . '/permit/detail/' . $id;
			break;
		case 'overtime':
			$detail_url = getGlobalVar('company')->slug . '/overtime/detail/' . $id;
			break;
		case 'training':
			$detail_url = getGlobalVar('company')->slug . '/training/detail/' . $id;
			break;
		case 'profile':
			$detail_url = getGlobalVar('company')->slug . '/profile/request/detail/' . $id;
			break;
		case 'photo_profile':
			$detail_url = getGlobalVar('company')->slug . '/profile/request/detail/' . $id;
			break;
		case 'address':
			$detail_url = getGlobalVar('company')->slug . '/address/request/detail/' . $id;
			break;
		case 'contact':
			$detail_url = getGlobalVar('company')->slug . '/contact/request/detail/' . $id;
			break;					
		default:						
			break;					
	}
	return $detail_url;
}

function urlRequestConfirmation($data, $id) {
	$detail_url = '';	
	switch ($data) {
		case 'leave':
			$detail_url = getGlobalVar('company')->slug . '/leave/confirmation/' . $id;
			break;
		case 'permit':
			$detail_url = getGlobalVar('company')->slug . '/permit/confirmation/' . $id;
			break;
		case 'overtime':
			$detail_url = getGlobalVar('company')->slug . '/overtime/confirmation/' . $id;
			break;
		case 'training':
			$detail_url = getGlobalVar('company')->slug . '/training/confirmation/' . $id;
			break;
		case 'profile':
			$detail_url = getGlobalVar('company')->slug . '/profile/request/confirmation/' . $id;
			break;
		case 'photo_profile':
			$detail_url = getGlobalVar('company')->slug . '/profile/request/confirmation/' . $id;
			break;
		case 'address':
			$detail_url = getGlobalVar('company')->slug . '/address/request/confirmation/' . $id;
			break;
		case 'contact':
			$detail_url = getGlobalVar('company')->slug . '/contact/request/confirmation/' . $id;
			break;					
		default:						
			break;					
	}
	return $detail_url;
}