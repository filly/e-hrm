<?php
function humanDate($str) {
	if ($time = strtotime($str)) {
		$y = date('Y', $time);
		$m = lang('_month')[date('m', $time)];
		$d = date('d', $time);
		$humanDate = lang('_human_date');
		$humanDate = str_replace(array('{Y}', '{m}', '{d}'), array($y, $m, $d), $humanDate);
		return $humanDate;
	} else {
		return null;
	}
}

function humanDateTime($str) {
	if ($time = strtotime($str)) {
		$y = date('Y', $time);
		$m = lang('_month')[date('m', $time)];
		$d = date('d', $time);
		$H = date('H', $time);
		$i = date('i', $time);
		$s = date('s', $time);
		$humanDate = lang('_human_datetime');
		$humanDate = str_replace(array('{Y}', '{m}', '{d}', '{H}', '{i}', '{s}'), array($y, $m, $d, $H, $i, $s), $humanDate);
		return $humanDate;
	} else {
		return null;
	}
}