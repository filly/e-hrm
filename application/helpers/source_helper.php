<?php
function sourceDivisions($placeholder = null) {    
    $CI = &get_instance();
    if (!$placeholder) {
      $placeholder = lang('chose') . ' ' . lang('division');
    }
    $records = $CI->db->where('company_id', getGlobalVar('company')->company_id)
                     ->get('divisions');
    return cmb($records, 'division_id', 'division', $placeholder); 
}

function sourceDepartments($placeholder = null) {
    $CI = &get_instance();
    if (!$placeholder) {
      $placeholder = lang('chose') . ' ' . lang('department');
    }
    $records = $CI->db->select('department_path.department_id, department_path')
                      ->join('(select 
                                  department_path.department_id, GROUP_CONCAT(department ORDER BY level ASC SEPARATOR " > ") AS department_path
                                FROM
                                  department_path
                                INNER JOIN departments ON departments.department_id = department_path.path 
                                GROUP BY department_path.department_id)department_path', 
                            'department_path.department_id = departments.department_id')
                      ->where('company_id', getGlobalVar('company')->company_id)                            
                      ->group_by('department_path.department_id') 
                      ->order_by('department_path', 'asc')                                          
                      ->get('departments');
    return cmb($records, 'department_id', 'department_path', $placeholder);    
}

function sourceClasses($placeholder = null) {
    $CI = &get_instance();
     if (!$placeholder) {
      $placeholder = lang('chose') . ' ' . lang('class');
    }
    $records = $CI->db->where('company_id', getGlobalVar('company')->company_id)
                      ->get('classes');
    return cmb($records, 'class_id', 'class', $placeholder);
}

function sourcePositions($placeholder = null) {
    $CI = &get_instance();
     if (!$placeholder) {
      $placeholder = lang('chose') . ' ' . lang('position');
    }
    $records = $CI->db->where('company_id', getGlobalVar('company')->company_id)
                      ->get('positions');
    return cmb($records, 'position_id', 'position', $placeholder);
}

function sourceLevel($placeholder = null) {
  $CI = &get_instance();
   if (!$placeholder) {
      $placeholder = lang('chose') . ' ' . lang('level');
    }
   $records = $CI->db->where('company_id', getGlobalVar('company')->company_id)
                     ->where('grade < ', getAuth('grade'))
                      ->get('level');
    return cmb($records, 'level_id', 'level', $placeholder);
}

function cmb($data, $valueField, $textField, $placeholder = null)
{
    $options[''] = ($placeholder) ? $placeholder : lang('chose');

    foreach ($data->result() as $row) {
        $options[$row->$valueField] = $row->$textField;
    }

    return $options;
}