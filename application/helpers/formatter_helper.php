<?php

function toDate($str) {
	if ($time = strtotime($str)) {
		return date(lang('_date'), $time);
	} else {
		return null;
	}
}

function toTime($str) {
	if ($time = strtotime($str)) {
		return date(lang('_time'), $time);
	} else {
		return null;
	}
}

function toDateTime($str) {
	if ($time = strtotime($str)) {
		return date(lang('_datetime'), $time);
	} else {
		return null;
	}
}

function toNumber($number) {
	$number = number_format($number);
	$number = str_replace(',', lang('_number_separator'), $number);
	return $number;
}

function toSelect($source, $value) {
	if (isset($source[$value])) {
		return $source[$value];
	} else {
		return null;
	}
}

function toBoolean($value) {
	return toSelect(lang('_boolean'), $value);
}

function antiNull($str, $to = '-') {
	if ($str) {
		return $str;
	} else {
		return $to;
	}
}

function toCurrency($str) {
	$str = number_format($str);
	$str = str_replace(',', lang('_thousand_separator'), $str);
	return lang('_currency').$str;
}