<?= getview('template/header') ?>
<div class="box box-primary profile-panel">
	<div class="box-body">
		<div class="row">
			<div class="col-md-2">
			 	<?php
			        if ($this->auth->get('photo')) {
			            $src = base_url('public/images/' . $this->auth->get('photo'));
			        } else {
			            $src = base_url('public/dist/img/noimage.jpg');
			        }
		        ?>
		        <img src="<?= $src ?>" class="img-responsive" />
			</div>
			<div class="col-md-10">
				<h3><?= $employee->first_name ?> <?= $employee->last_name ?></h3>
				<div class="row">
					<div class="col-md-4">
						<p><?= lang('employee_code') ?> : <?= $employee->employee_code ?></p>					
						<div class="profile-list">						
							<?= lang('class') ?> <span class="pull-right"><?= $employee->class_code ?> - <?= $employee->class ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('position') ?> <span class="pull-right"><?= $employee->position_code ?> - <?= $employee->position ?></span>						
						</div>										
						<div class="profile-list">						
							<?= lang('level') ?> <span class="pull-right"><?= $employee->level ?></span>						
						</div>	
					</div>
					<div class="col-md-4">					
						<div class="profile-list">						
							<?= lang('hire_date') ?> <span class="pull-right"><?= humanDate($employee->hire_date) ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('division') ?> <span class="pull-right"><?= $employee->division_code ?> - <?= $employee->division ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('department') ?> <span class="pull-right"><?= $employee->department_code ?> - <?= $employee->department ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('employee_status') ?> <span class="pull-right"><?= toSelect(lang('_employee_status'), $employee->employee_status) ?></span>						
						</div>																
					</div>
				</div>			
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-4">
				<div class="profile-list">						
					<?= lang('gender') ?> <span class="pull-right"><?= toSelect(lang('_gender'), $employee->gender) ?></span>						
				</div>	
				<div class="profile-list">						
					<?= lang('born') ?> <span class="pull-right"><?= $employee->born_place ?>, <?= humanDate($employee->born_date) ?></span>						
				</div>
				<div class="profile-list">						
					<?= lang('age') ?>
					<span class="pull-right">
						<?php
							$born = Carbon\Carbon::createFromDate(
								date('Y', strtotime($employee->born_date)),
								date('m', strtotime($employee->born_date)),
								date('d', strtotime($employee->born_date))
							);							
						?>
						<?= $born->age ?> <?= lang('years_old') ?>
					</span>						
				</div>	
				<div class="profile-list">						
					<?= lang('height') ?>/<?= lang('weight') ?> 
					<span class="pull-right">
						<?= $employee->height ?> <?= lang('_height_class') ?> / <?= $employee->weight ?> <?= lang('_weight_class') ?>
					</span>						
				</div>	
				<div class="profile-list">						
					<?= lang('religion') ?> <span class="pull-right"><?= toSelect(lang('_religion'), $employee->religion) ?></span>						
				</div>
				<div class="profile-list">						
					<?= lang('blood_type') ?> <span class="pull-right"><?= toSelect(lang('_blood_type'), $employee->blood_type) ?></span>						
				</div>
				<div class="profile-list">						
					<?= lang('nationality') ?> <span class="pull-right"><?= $employee->nationality ?></span>						
				</div>
				<div class="profile-list">						
					<?= lang('marital_status') ?> <span class="pull-right">
						<?= toBoolean($employee->marital_status) ?>
						<?php if ($employee->marital_status == 'married') { ?>
							/ <?= humanDate($employee->married_date) ?>
						<?php } ?>
					</span>						
				</div>
			</div>
		</div>
		<div class="box-sub-title">
			<h3><?= lang('contact_information') ?></h3>
		</div>
		<div class="row">
			<div class="col-md-3">
				<h4><?= lang('addresses') ?></h4>				
				<?php foreach ($addresses->result() as $address) { ?>
					<div class="profile-list">						
						<?= $address->description ?> (<?= toSelect(lang('_home_status'), $address->home_status) ?>)<br />
						<span>
							<?= $address->address ?><br />
							<?= $address->city ?> - <?= $address->zip_code ?><br />
							<?= lang('phone') ?> : <?= antiNull($address->phone) ?>
						</span>						
					</div>						
				<?php } ?>				
			</div>			
			<div class="col-md-3">
				<h4><?= lang('phones') ?></h4>				
				<?php foreach ($mobiles->result() as $phone) { ?>
					<div class="profile-list">						
						<?= toSelect(lang('_contact_type'), $phone->contact_type) ?> <span class="pull-right"><?= $phone->description ?></span>						
					</div>
				<?php } ?>				
			</div>
			<div class="col-md-3">
				<h4><?= lang('mobiles') ?></h4>
				<?php foreach ($mobiles->result() as $mobile) { ?>
					<div class="profile-list">						
						<?= toSelect(lang('_contact_type'), $mobile->contact_type) ?> <span class="pull-right"><?= $mobile->description ?></span>						
					</div>
				<?php } ?>				
			</div>
			<div class="col-md-3">
				<h4><?= lang('emails') ?></h4>				
				<?php foreach ($emails->result() as $email) { ?>
					<div class="profile-list">						
						<?= toSelect(lang('_contact_type'), $email->contact_type) ?> <span class="pull-right"><?= $email->description ?></span>						
					</div>
				<?php } ?>				
			</div>
		</div>
		<div class="box-sub-title">
			<h3><?= lang('family_information') ?></h3>
		</div>		
		<?php foreach ($families as $family) { ?>
		<div class="row">			
			<div class="col-md-3">
				<b><?= $family->name ?></b>
				<div class="profile-list">						
					<?= lang('relationship') ?> <span class="pull-right"><?= toSelect(lang('_relationship'), $family->relationship) ?></span>
				</div>
				<div class="profile-list">
					<?= lang('born') ?> <span class="pull-right"><?= $family->born_place ?>, <?= humanDate($family->born_date) ?></span>
				</div>
				<div class="profile-list">					
					<?= lang('age') ?>
					<span class="pull-right">
						<?php
							$born = Carbon\Carbon::createFromDate(
								date('Y', strtotime($employee->born_date)),
								date('m', strtotime($employee->born_date)),
								date('d', strtotime($employee->born_date))
							);							
						?>
						<?= $born->age ?> <?= lang('years_old') ?>
					</span>
				</div>
				<div class="profile-list">									
					<?= lang('marital_status') ?> <span class="pull-right"><?= toBoolean($family->marital_status) ?>
						<?php if ($family->marital_status == 'true') { ?>
							/ <?= humanDate($family->married_date) ?>
						<?php } ?>
					</span>																												
				</div>									
			</div>
			<div class="col-md-3">
				<b><?= lang('addresses') ?></b>
				<div class="profile-list">							
					<?php foreach ($family->addresses->result() as $family_address) { ?>
						<div class="profile-list">						
							<?= $family_address->description ?> (<?= toSelect(lang('_home_status'), $family_address->home_status) ?>)<br />									
							<?= $family_address->address ?><br />
							<?= $family_address->city ?> - <?= $family_address->zip_code ?><br />
							<?= lang('phone') ?> : <?= antiNull($family_address->phone) ?>									
						</div>						
					<?php } ?>						
				</div>					
			</div>
			<div class="col-md-3">
				<b><?= lang('contacts') ?></b>					
				<div class="profile-list">	
				<?php foreach ($family->phones->result() as $family_phone) { ?>
					<span class="fa fa-phone fa-fw"></span> <?= $family_phone->description ?> (<?= toSelect(lang('_contact_type'), $family_phone->contact_type) ?>)<br />
				<?php } ?>
				</div>
				<div class="profile-list">	
				<?php foreach ($family->mobiles->result() as $family_mobile) { ?>
					<span class="fa fa-mobile fa-fw"></span> <?= $family_mobile->description ?> (<?= toSelect(lang('_contact_type'), $family_mobile->contact_type) ?>)<br />
				<?php } ?>
				</div>
				<div class="profile-list">	
				<?php foreach ($family->emails->result() as $family_email) { ?>
					<span class="fa fa-envelope fa-fw"></span> <?= $family_email->description ?> (<?= toSelect(lang('_contact_type'), $family_email->contact_type) ?>)<br />
				<?php } ?>
				</div>					
			</div>
			<div class="col-md-3">
				<b><?= lang('job') ?></b>
				<div class="profile-list">	
					<?= lang('occupation') ?> <span class="pull-right"><?= $family->occupation ?></span>
				</div>
				<div class="profile-list">	
					<?= lang('workplace') ?> <span class="pull-right"><?= $family->workplace ?></span>
				</div>
				<div class="profile-list">						
					<?= lang('employee_code') ?> <span class="pull-right"><?= $family->employee_code ?></span>
				</div>
				<b><?= lang('educations') ?></b>
			</div>
		</div>
		<hr>
		<?php } ?>		
		<div class="row">
			<div class="col-md-4">
				<div class="box-sub-title">
					<h3><?= lang('education_information') ?></h3>					
				</div>
				<?php foreach ($educations->result() as $education) { ?>
					<b>
						<?= $education->institution ?>
						<?= ($education->major) ? ' - ' . $education->major : '' ?>
					</b>			
					<div class="profile-list">
						<span><?= $education->certificate_id ?></span><br />
						<?php if ($education->graduated == 'true') { ?>
							<?= lang('graduation_date') ?> <span class="pull-right"><?= humanDate($education->graduation_date) ?></span><br />
							<?= lang('result') ?> <span class="pull-right"><?= $education->result ?></span>
						<?php } ?>
					</div>		
					<hr />
				<?php } ?>
			</div>
			<div class="col-md-4">
				<div class="box-sub-title">
					<h3><?= lang('experience_information') ?></h3>
				</div>
				<?php foreach ($experiences->result() as $experience) { ?>
					<b>
						<?= $experience->workplace ?>						
					</b>			
					<div class="profile-list">												
						<?= lang('occupation') ?> <span class="pull-right"><?= $experience->occupation ?></span><br />
						<?= lang('period') ?> <span class="pull-right">
							<?= humanDate($experience->start_work_date) ?> <?= lang('_to') ?> <?= humanDate($experience->end_work_date) ?>							
						</span>					
					</div>
					<hr />		
				<?php } ?>
			</div>
			<div class="col-md-4">
				<div class="box-sub-title">
					<h3><?= lang('identity_cards') ?></h3>
				</div>
				<?php foreach ($identityCards->result() as $identityCard) { ?>
					<b><?= $identityCard->description ?> - <?= $identityCard->identity_card_id ?></b>
					<div class="profile-list">		
						<?= lang('published') ?> : <?= $identityCard->published ?><br />
						<?= lang('expired') ?> : <?= humanDate($identityCard->expired) ?><br />
					</div>
					<hr />
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?= getview('template/footer') ?>