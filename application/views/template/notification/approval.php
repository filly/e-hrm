<li class="dropdown notifications-menu" data-notifications="<?= getGlobalVar('company')->slug ?>/services/notifications/approval">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-bell-o"></i>      
    </a>
    <ul class="dropdown-menu">      
      <li>
      	<ul class="menu" data-template="#list-notification-approval-template"></ul>
      </li>      
    </ul>
</li>
<script id="list-notification-approval-template" type="text/x-handlebars-template">
{{#list data}}
	<li>
		<a href="{{detail_url}}">
			<b>{{request_type}} {{data}}</b><br>
			{{{request_status}}}<br>			
			<small><?= lang('confirmed_by') ?> <span class="text-info">{{confirmed_by}}</span> <i class="fa fa-clock-o"></i> {{since}}</small>
		</a>
	</li>
{{/list}}      			
</script>