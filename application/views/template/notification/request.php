<li class="dropdown notifications-menu" data-notifications="<?= getGlobalVar('company')->slug ?>/services/notifications/request">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-refresh"></i>      
    </a>
    <ul class="dropdown-menu">      
      <li>
      	<ul class="menu" data-template="#list-notification-request-template"></ul>
      </li>
      <li class="footer"><a href="<?= base_url(getGlobalVar('company')->slug . '/request') ?>"><?= lang('view_all') ?></a></li>      
    </ul>
</li>
<script id="list-notification-request-template" type="text/x-handlebars-template">
{{#list data}}
	<li>
		<a href="{{detail_url}}">
			<b>{{first_name}} {{last_name}}</b><br>
			{{request_type}} {{data}}
			<small><i class="fa fa-clock-o"></i> {{since}}</small>
		</a>
	</li>
{{/list}}      			
</script>