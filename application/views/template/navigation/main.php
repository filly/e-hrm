<?php
if (isSignedIn()) {
	switch (getAuth('role')) {
		case 'employee':
			getview('template/navigation/main/employee');
			break;
		case  'manager':
			getview('template/navigation/main/manager');
			break;
		default:
			# code...
			break;
	}
} else {

}