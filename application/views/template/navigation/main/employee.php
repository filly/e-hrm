<li><a href="<?= base_url(getGlobalVar('company')->slug) ?>"><span class="glyphicon glyphicon-dashboard"></span> <?= lang('dashboard') ?></a></li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-pencil"></span> <?= lang('request') ?></a>
    <ul class="dropdown-menu">
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/leave/request') ?>"><?= lang('leave') ?></a></li>        
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/permit/request') ?>"><?= lang('permit') ?></a></li>
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/overtime/request') ?>"><?= lang('overtime') ?></a></li>
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/training/request') ?>"><?= lang('training') ?></a></li>
    </ul>
</li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?= lang('personal_data') ?></a>
    <ul class="dropdown-menu">
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/profile') ?>"><?= lang('profile') ?></a></li>        
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/contacts') ?>"><?= lang('contacts') ?></a></li>
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/addresses') ?>"><?= lang('addresses') ?></a></li>        
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/families') ?>"><?= lang('families') ?></a></li>        
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/educations') ?>"><?= lang('educations') ?></a></li>
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/experiences') ?>"><?= lang('experiences') ?></a></li>        
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/identity_cards') ?>"><?= lang('identity_cards') ?></a></li>
        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/documents') ?>"><?= lang('documents') ?></a></li>
    </ul>
</li>
