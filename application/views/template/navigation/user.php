<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <?php
        if ($this->auth->get('photo')) {
            $src = base_url('public/images/' . $this->auth->get('photo'));
        } else {
            $src = base_url('public/dist/img/noimage.jpg');
        }
        ?>
        <img src="<?= $src ?>" class="user-image" alt="User Image"/>
        <span class="hidden-xs"><?= getAuth('first_name') ?></span>
    </a>
    <ul class="dropdown-menu">
    <li><a href="<?= base_url(getGlobalVar('company')->slug . '/profile') ?>"><span class="glyphicon glyphicon-cog"></span> <?= lang('setting') ?></a></li>
        <li role="separator" class="divider"></li>
        <li><a href="<?= base_url('signout') ?>"><span class="glyphicon glyphicon-off"></span> <?= lang('signout') ?></a></li>
    </ul>
</li>