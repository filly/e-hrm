</section>
</div>
</div>
<footer class="main-footer">
    <div class="container">       
        &copy 2015 HumanCapital
    </div>
</footer>
</div>
<script src="<?= base_url('public/plugins/jQuery/jQuery-2.1.4.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('public/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('public/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<script src="<?= base_url('public/plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('public/plugins/datepicker/js/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('public/plugins/timepicker/js/bootstrap-timepicker.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('public/plugins/moment/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('public/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?= base_url('public/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="<?= base_url('public/plugins/handlebars/handlebars.js') ?>"></script>
<script src="<?= base_url('public/dist/js/app.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('public/dist/js/notifications.js') ?>" type="text/javascript"></script>
<?php render('script') ?>
</body>
</html>