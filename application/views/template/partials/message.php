<?php if ($this->session->flashdata('successMessage')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('successMessage') ?></h4>
    </div>
<?php } elseif ($this->session->flashdata('errorMessage')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-exclamation-circle"></i> <?= $this->session->flashdata('errorMessage') ?></h4>
    </div>
<?php } ?>