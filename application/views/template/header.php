<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        <?= lang('_title') ?>        
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>    
    <link href="<?= base_url('public/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/plugins/fontawesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/plugins/select2/select2.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/plugins/datepicker/css/bootstrap-datepicker3.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/plugins/timepicker/css/bootstrap-timepicker.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/dist/css/AdminLTE.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/dist/css/app.css') ?>" rel="stylesheet" type="text/css"/>
    <?php render('css') ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="../../index2.html"
                       class="navbar-brand"><b><?= lang('_title') ?></b></a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?php getview('template/navigation/main') ?>
                    </ul>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">    
                        <?php getview('template/notification/main') ?>                    
                        <?php if (isSignedIn()) {
                            getview('template/navigation/user');
                        } ?>                     
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="content-wrapper">
        <div class="container">
            <section class="content">                  