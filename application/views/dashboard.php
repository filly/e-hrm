<?php getview('template/header') ?>
<?php getview('template/partials/message') ?>
<div class="box box-primary profile-panel">
	<div class="box-body">
		<div class="row">
			<div class="col-md-2">
			 	<?php
			        if ($this->auth->get('photo')) {
			            $src = base_url('public/images/' . $this->auth->get('photo'));
			        } else {
			            $src = base_url('public/dist/img/noimage.jpg');
			        }
		        ?>
		        <img src="<?= $src ?>" class="img-responsive" />
			</div>
			<div class="col-md-10">
				<h3><?= $employee->first_name ?> <?= $employee->last_name ?></h3>
				<div class="row">
					<div class="col-md-4">
						<p><?= lang('employee_code') ?> : <?= $employee->employee_code ?></p>					
						<div class="profile-list">						
							<?= lang('class') ?> <span class="pull-right"><?= $employee->class_code ?> - <?= $employee->class ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('position') ?> <span class="pull-right"><?= $employee->position_code ?> - <?= $employee->position ?></span>						
						</div>										
						<div class="profile-list">						
							<?= lang('level') ?> <span class="pull-right"><?= $employee->level ?></span>						
						</div>	
					</div>
					<div class="col-md-4">					
						<div class="profile-list">						
							<?= lang('hire_date') ?> <span class="pull-right"><?= humanDate($employee->hire_date) ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('division') ?> <span class="pull-right"><?= $employee->division_code ?> - <?= $employee->division ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('department') ?> <span class="pull-right"><?= $employee->department_code ?> - <?= $employee->department ?></span>						
						</div>	
						<div class="profile-list">						
							<?= lang('employee_status') ?> <span class="pull-right"><?= toSelect(lang('_employee_status'), $employee->employee_status) ?></span>						
						</div>																
					</div>
				</div>			
			</div>
		</div>
		<hr />
		<div class="row">
		<div class="col-md-6">
		<div class="box-sub-title">
			<h3><?= lang('waiting_request') ?> (<?= $requests->num_rows() ?>)</h3>
		</div>
		<table class="table table-hover">
			<?php foreach ($requests->result() as $request) { ?>
				<tr>
					<td width="150"><?= humanDate($request->created_at) ?></td>
					<td>
						<?= toSelect(lang('_request_type'), $request->request_type) ?> - <?= toSelect(lang('_request_data'), $request->data) ?> <br />						
					</td>			
					<td width="100" class="text-right">								
						<?= anchor(urlRequestDetail($request->data, $request->request_id), '<i class="fa fa-search"></i>', 'class="text-info"') ?>						
					</td>		
				</tr>
			<?php } ?>
		</table>
		</div>
		</div>
	</div>
</div>
<?php getview('template/footer') ?>