<?php getview('template/header') ?>
<div id="signin-wrapper">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?= lang('signin_app_title') ?> <a href="#" class="brand"><?= getGlobalVar('company')->company_name ?></a></h3>			
		</div>
		<div class="box-body">
        <?= getview('template/partials/message') ?>
        <?= getview('template/partials/validation') ?>
        <?= $this->form->open('', 'class="form-horizontal"') ?>                	 
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('username') ?></label>
                <div class="col-md-9">
                    <?= $this->form->text('username', null, 'placeholder="'.lang('enter_username').'" class="form-control"') ?>                        
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('password') ?></label>
                <div class="col-md-9">
                    <?= $this->form->password('password', null, 'placeholder="'.lang('enter_password').'" class="form-control"') ?>                                    
                </div>
            </div>
            <div class="form-group">                        
                  <div class="col-md-offset-3 col-md-9">
                        <?= $this->form->submit('btn_signin', lang('signin'), 'class="btn btn-success"') ?>                                                
                  </div>
            </div>            
      <?= $this->form->close() ?>
		</div>				
	</div>
</div>
<?php getview('template/footer') ?>