<?php

$lang = array(

	'_date' => 'd-m-Y',
	'_time' => 'H:i:s',
	'_datetime' => 'd-m-Y H:i:s',
	'_human_date' => '{d} {m} {Y}',
	'_human_datetime' => '{d} {m} {Y} {H}:{i}',

	'_currency' => 'Rp ',
	'_thousand_separator' => '.',

	'_number_separator' => '.',

	'_to' => 's/d',

	'_chose' => '-Pilih-',

	'_weight_class' => 'Kg',
    '_height_class' => 'Cm',

	'_month' => array(
		'01' => 'Januari',
		'02' => 'Febuari',
		'03' => 'Maret',
		'04' => 'April',
		'05' => 'Mei',
		'06' => 'Juni',
		'07' => 'Juli',
		'08' => 'Agustus',
		'09' => 'September',
		'10' => 'Oktober',
		'11' => 'November',
		'12' => 'Desember'
	)

);