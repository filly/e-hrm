<?php

$lang = array(

	'_title' => 'HumanCapital',
	'add' => 'Tambah',
	'edit' => 'Ubah',	
	'remove' => 'Hapus',
	'save' => 'Simpan',
	'save_changes' => 'Simpan Perubahan',
	'reset' => 'Reset',
	'cancel' => 'Batalkan',
	'send' => 'Kirim',
	'send_request' => 'Kirim Pemintaan',
	'submit' => 'Submit',
	'approve' => 'Setuju',	
	'reject' => 'Tolak',
	'suspend' => 'Tangguhkan',
	'detail' => 'Detail',
	'view_all' => 'Lihat Semua',
	'back' => 'Kembali',
	'chose' => 'Pilih',
	'download' => 'Unduh',

	'signin_title' => 'Sign In <a href="#" class="brand"><span class="black">Human</span><span class="blue">Capital</span></a>',
	'signup_title' => 'Sign Up <a href="#" class="brand"><span class="black">Human</span><span class="blue">Capital</span></a>',	

	'need_account' => 'Belum punya akun? <a href="'.base_url('signup').'">Sign Up disini</a>',
	'forgot_password' => 'Lupa password ? <a href="'.base_url('forgotpassword').'">Klik disini</a>',
	'have_account' => 'Sudah punya akun? <a href="'.base_url('signin').'">Sign In disini</a>',

	'signin_app_title' => 'Sign In',	
	'waiting_approval' => 'Menunggu Persetujuan',
	'manage_personal_data' => 'Kelola Data Diri',
	'waiting_request' => 'Permintaan',
	'username' => 'Username',
	'email' => 'Email',
	'password' => 'Password',
	'retype_password' => 'Ulangi Password',
	'name' => 'Nama',
	'first_name' => 'Nama depan',
	'last_name' => 'Nama belakang',
	'employee_code' => 'NIK',
	'division' => 'Divisi',
	'department' => 'Departemen',
	'class' => 'Golongan',
	'position' => 'Jabatan',
	'level' => 'Tingkat',
	'hire_date' => 'Tanggal Perekrutan',
	'employee_status' => 'Status',
	'dashboard' => 'Dashboard',
	'signin' => 'Sign In',
	'signup' => 'Sign Up',
	'signout' => 'Sign Out',	
	'setting' => 'Pengaturan',
	'request' => 'Permintaan',
	'my_request' => 'Permintaanku',
	'leave' => 'Cuti',
	'overtime' => 'Lembur',
	'training' => 'Pelatihan',
	'personal_data'	=> 'Data Diri',
	'profile' => 'Profil',
	'contacts' => 'Kontak',
	'addresses' => 'Alamat',
	'address' => 'Alamat',
	'families' => 'Keluarga',
	'family' => 'Keluarga',
	'identity_cards' => 'Kartu Identitas',
	'identity_card' => 'Kartu Identitas',
	'experiences' => 'Pengalaman',
	'educations' => 'Pendidikan',
	'documents' => 'Berkas',
	'contact_information' => 'Informasi Kontak',
	'phone' => 'Telpon',
	'phones' => 'Telpon',
	'mobiles' => 'HP',
	'emails' => 'Email',
	'gender' => 'Jenis Kelamin',
	'born' => 'Kelahiran',
	'height' => 'Tinggi',
	'weight' => 'Berat',
	'nationality' => 'Kewarganegaraan',
	'religion' => 'Agama',
	'blood_type' => 'Golongan Darah',
	'marital_status' => 'Status Pernikahan',
	'age' => 'Umur',
	'years_old' => 'Tahun',
	'relationship' => 'Hubungan',
	'job' => 'Pekerjaan',
	'occupation' => 'Jabatan',
	'workplace' => 'Tempat Kerja',
	'start_work_date' => 'Tanggal Mulai Bekerja',
	'end_work_date' => 'Tanggal Selesai Bekerja',
	'grade' => 'Tingkat',
	'major' => 'Jurusan',
	'institution' => 'Institusi',
	'certificate_id' => 'Nomor Ijazah',
	'graduated' => 'Lulus',
	'graduation_date' => 'Tanggal Kelulusan',
	'result' => 'Hasil',	
	'period' => 'Periode',
	'married_date' => 'Tanggal Pernikahan',
	'married' => 'Pernikahan',	
    'family_information' => 'Informasi Keluarga',
    'education_information' => 'Informasi Pendidikan',
    'experience_information'=> 'Informasi Pengalaman',
	'identity_card_id' => 'ID Kartu Identitas',
    'expired' => 'Masa berlaku',
    'published' => 'Dikeluarkan',
    'identity' => 'Identitas',
    'media' => 'Media',
    'contact_type' => 'Jenis Kontak',
    'contact'  => 'Kontak',
    'description' => 'Keterangan',
    'city' => 'Kota',
    'zip_code' => 'Kode Pos',
    'home_status' => 'Status Rumah',
	'document' => 'Berkas',
	'document_id' => 'ID Berkas',
	'document_file' => 'File',
    'request_overtime' => 'Permintaan Lembur',
    'request_training' => 'Permintaan Pelatihan',
    'overtime_date' => 'Tanggal Lembur',
    'start_overtime' => 'Mulai Lembur',
    'end_overtime' => 'Selesai Lembur',
    'time_overtime' => 'Lama Lembur',
    'reason' => 'Alasan',
    'training' => 'Pelatihan',
    'training_date' => 'Tanggal Pelatihan',
    'cost' => 'Biaya',
    'place' => 'Tempat',
    'trainer' => 'Penyelenggara',
    'general' => 'Umum',
    'history' => 'Riwayat',
    'menu' => 'Menu',
    'training_start_date' => 'Mulai Pelatihan',
    'training_end_date' => 'Selesai Pelatihan',
    'created_at' => 'Dibuat Tanggal',    
    'num_of_days' => 'Lama (hari)',
    'permit' => 'Izin',
    'permit_date' => 'Tanggal Izin',
    'permit_type' => 'Jenis Izin',
    'permit_time' => 'Jam',    
    'request_permit' => 'Permintaan Izin', 	    
    'employee_code' => 'NIK',    
    'request_status' => 'Status Permintaan',
    'confirmation' => 'Konfirmasi',
    'confirmed_by' => 'Dikonfirmasi Oleh',
    'confirmed_at' => 'Konfirmasi Pada Tanggal',
    'employees' => 'Karyawan',
    'num_of_days' => 'Lama',
    'days' => 'Hari',
    'list_request' => 'Daftar Permintaan',
    'team_overtime' => 'Lembur Team',
    'team_training' => 'Pelatihan Team',    
	'enter_username' => 'Masukan username',
	'active' => 'Aktif',
	'not_married' => 'Belum menikah',
	'since' => 'Sejak',
	'profession' => 'Pekerjaan',
	'still_in_education' => 'Masih dalam pendidikan',

	'enter_email' => 'Masukan email',
	'enter_password' => 'Masukan password',
	'enter_retype_password' => 'Ulangi password',
	'enter_first_name' => 'Masukan nama depan',
	'enter_last_name' => 'Masukan nama belakang',
	'enter_name' => 'Masukkan nama',
	'enter_born_place' => 'Masukan tempat Lahir',
	'enter_born_date' => 'Pilih tanggal Lahir',
	'enter_married_date'=> 'Pilih tanggal menikah',
	'enter_nationality' => 'Masukan kewarganegaraan',
	'enter_description' => 'Masukan keterangan',
	'enter_contact' => 'Masukan kontak',
	'enter_address' => 'Masukan alamat',
	'enter_city' => 'Masukan kota',
	'enter_zip_code' => 'Masukan kode Pos',
	'enter_phone' => 'Masukan telepon',
	'enter_start_overtime' => 'Masukan jam mulai',
    'enter_end_overtime' => 'Masukan jam selesai',
    'enter_reason' => 'Masukan alasan',
    'enter_training' => 'Masukan pelatihan',
    'enter_training_start_date' => 'Pilih tanggal mulai pelatihan',
    'enter_training_end_date' => 'Pilih tanggal selesai pelatihan',
    'enter_cost' => 'Masukan biaya',
    'enter_place' => 'Masukan tempat pelaksanaan',
    'enter_trainer' => 'Masukan penyelenggara',
    'enter_permit_date' => 'Pilih tanggal izin',    
    'enter_permit_time' => 'Pilih Jam',

	'enter_grade' => 'Masukkan tingkat',
	'enter_major' => 'Masukkan jurusan',
	'enter_institution' => 'Masukkan institusi',
	'enter_certificate_id' => 'Masukkan nomor ijazah',
	'enter_graduation_date' => 'Masukkan tanggal kelulusan',
	'enter_result' => 'Masukkan hasil',
	'enter_workplace' => 'Masukkan tempat kerja',
	'enter_occupation' => 'Masukkan jabatan',
	'enter_start_work_date' => 'Masukkan tanggal mulai bekerja',
	'enter_end_work_date' => 'Masukkan tanggal selesai bekerja',
	'enter_identity_card_id' => 'Masukkan ID kartu identitas',
	'enter_published' => 'Masukkan tanggal terbit',
	'enter_expired' => 'Masukkan masa berlaku',
	'enter_profession' => 'Masukkan pekerjaan',

	'success_signup' => 'Akun berhasil dibuat.',
	'success_request_changes' => 'Berhasil melakukan permintaan perubahan.',
	'success_cancel_request_changes' => 'Berhasil membatalkan permintaan perubahan.',
	'success_send_request_overtime' => 'Berhasil mengirim permintaan lembur.',
	'success_send_request_training' => 'Berhasil mengirim permintaan pelatihan.',
	'success_cancel_request_training' => 'Berhasil membatalkan permintaan pelatihan.',
	'success_cancel_request_overtime' => 'Berhasil membatalkan permintaan lembur.',
	'success_send_request_permit' => 'Berhasil mengirim permintaan izin.',
	'success_cancel_request_permit' => 'Berhasil membatalkan permintaan izin.',
	'success_approve_request_permit' => 'Permintaan izin telah disetujui.',
	'success_reject_request_permit' => 'Permintaan izin telah ditolak.',
	'success_suspend_request_permit' => 'Permintaan izin berhasil ditangguhkan.',
	'success_approve_request_overtime' => 'Permintaan lembur telah disetujui.',
	'success_reject_request_overtime' => 'Permintaan lembur telah ditolak.',
	'success_suspend_request_overtime' => 'Permintaan lembur berhasil ditangguhkan.',
	'success_approve_request_training' => 'Permintaan pelatihan telah disetujui.',
	'success_reject_request_training' => 'Permintaan pelatihan telah ditolak.',
	'success_suspend_request_training' => 'Permintaan pelatihan berhasil ditangguhkan.',
	'success_approve_request_leave' => 'Permintaan cuti telah disetujui.',
	'success_reject_request_leave' => 'Permintaan cuti telah ditolak.',
	'success_suspend_request_leave' => 'Permintaan cuti telah ditangguhkan.',


	'error_form_validation' => 'Form tidak terisi dengan benar',
	'error_signin' => 'Username atau password salah.',
	'error_no_file_uploaded' => 'Error no file uploaded.',
	'error_request_changes' => 'Gagal mengirim permintaan perubahan.',
	'error_cancel_request_changes' => 'Gagal membatalkan permintaan perubahan.',
	'error_send_request_overtime' => 'Gagal membatalkan permintaan lembur.',
	'error_send_request_training' => 'Gagal mengirim permintaan pelatihan.',
	'error_cancel_request_training' => 'Gagal membatalkan permintaan pelatihan.',
	'error_cancel_request_overtime' => 'Gagal membatalkan permintaan lembur.',
	'error_send_request_permit' => 'Gagal mengirim permintaan lembur.',
	'error_cancel_request_permit' => 'Gagal membatalkan permintaan lembur.',
	'error_approve_request_permit' => 'Permintaan izin telah gagal disetujui.',
	'error_reject_request_permit' => 'Permintaan izin telah gagal ditolak.',
	'error_suspend_request_permit' => 'Permintaan izin gagal ditangguhkan.',
	'error_approve_request_overtime' => 'Permintaan lembur gagal disetujui.',
	'error_reject_request_overtime' => 'Permintaan lembur gagal ditolak.',
	'error_suspend_request_overtime' => 'Permintaan lembur gagal ditangguhkan.',
	'error_approve_request_training' => 'Permintaan pelatihan gagal disetujui.',
	'error_reject_request_training' => 'Permintaan pelatihan gagal ditolak.',
	'error_suspend_request_training' => 'Permintaan pelatihan gagal ditangguhkan.',
	'error_approve_request_leave' => 'Permintaan cuti gagal disetujui.',
	'error_reject_request_leave' => 'Permintaan cuti gagal ditolak.',
	'error_suspend_request_leave' => 'Permintaan cuti gagal ditangguhkan.',
	'enter_document' => 'Masukkan nama berkas',
	'enter_document_id' => 'Masukkan ID berkas',

	'_boolean' => array(
        'true' => 'Ya',
        'false' => 'Tidak'
    ),

    '_religion' => array(
    	'' => 'Pilih Agama',
        'muslim' => 'Islam',
        'hindu' => 'Hindu',
        'buddha' => 'Buddha',
        'christian' => 'Kristen',
        'catholic' => 'Katolik',
        'other' => 'Lainya'
    ),

	'_gender' => array(
		'' => 'Pilih Gender',
		'male' => 'Laki-laki',
		'female' => 'perepmpuan',
		'other' => 'Lainya'
	),

	'_employee_status' => array(
		'' => 'Pilh Status Karyawan',
		'contract' => 'Kontrak',
		'permanent' => 'Tetap'
	),
	'_home_status' => array(
		'' => 'Pilih Status Rumah',
		'parent_home' => 'Rumah Orang Tua',
		'contract' => 'Kontrak / Kos',
		'private' => 'Rumah Pribadi'
	),
	'_contact_type' => array(
		'' => 'Pilih Jenis Kontak',
		'company' => 'Perusahaan',
		'private' => 'Pribadi',
		'home' => 'Rumah'
	),
	'_education' => array(
		'' => 'Pilih Pendidikan',
        'elementary_school' => 'SD',
        'junior_high_school' => 'SMP',
        'high_school' => 'SMA/SMK',
        'undergraduate' => 'S1',
        'graduate' => 'S2',
        'postgraduate' => 'S3'
    ),
    '_relationship' => array(
    	'' => 'Pilih Hubungan Keluarga',
        'father' => 'Ayah',
        'mother' => 'Ibu',
        'husband' => 'Suami',
        'wife' => 'Istri',
		'child' => 'Anak',
        'brother' => 'Saudara Laki-laki',
        'sister' => 'Saudara Perempuan'
    ),
     '_quit_type' => array(
     	'' => 'Pilih Jenis Keluar',
        'fired' => 'Dipecat',
        'resign' => 'Mengundurkan diri'
    ),

    '_blood_type' => array(
    	'' => 'Pilih Gol.Darah',
    	'O' => 'O',
    	'A' => 'A',
    	'B' => 'b',
    	'AB' => 'AB'
    ),
    '_marital_status' => array(
    	'' => 'Pilih Status Pernikahan',
    	'single' => 'Single',
    	'married' => 'Menikah'
    ),
    '_media' => array(
    	'' => 'Pilih Media',
    	'phone' => 'Telpon',
    	'mobile' => 'HP',
    	'email' => 'Email'
    ),

    '_request_type' => array(
    	'' => 'Pilih Jenis Permintaan',
    	'add' => 'Tambah',
    	'update' => 'Perbarui',
    	'remove' => 'Hapus',
    	'request' => 'Pengajuan',
    ),    
    '_request_data' => array(
    	'' => 'Pilih Jenis Data',
    	'address' => 'Alamat',
    	'contact' => 'Kontak',
    	'profile' => 'Profil',
		'education' => 'Pendidikan',
		'experience' => 'Pengalaman',
		'family' => 'Keluarga',
		'identity_card' => 'Kartu Identitas',
    	'photo_profile' => 'Foto Profil',
    	'leave' => 'Cuti',
    	'overtime' => 'Lembur',
    	'training' => 'Pelatihan',
    	'permit' => 'Izin'
    ),
    '_request_status' => array(
    	'' => 'Pilih Status Permintaan',
    	'waiting' => 'Menunggu',
    	'rejected' => 'Ditolak',
    	'approved' => 'Disetujui',
    	'canceled' => 'Dibatalkan'
    ),
    '_permit_type' => array(
    	'' => 'Pilih Jenis Izin',
    	'late' => 'Terlambat',
    	'go_home' => 'Pulang',
    	'go_out' => 'Keluar Kantor',
    	'company_business' => 'Urusan Perusahaan'
    )


);