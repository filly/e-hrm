<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indonesia {

	protected $CI;

	protected $model;

	public function __construct() {
		$this->CI = &get_instance();
		$this->CI->load->model('leave/indonesia_model');
		$this->CI->load->language('leave/indonesia');
		$this->model = $this->CI->indonesia_model;
	}

	public function index() {
		if ($this->CI->input->is_ajax_request()) {
			$this->CI->load->library('datatables');			

			$response = $this->CI->datatables->collection(function(){
				if ($filter = $this->CI->input->get('filter')) {				
					if ($filter['request_status'] <> '') {
						$this->CI->db->where('leave_status', $filter['request_status']);
					}
				}
				$this->CI->db->where('employee_id', getAuth('employee_id'))							
				->from('leave');
			})
			->showColumns('created_at, leave_start_date, leave_end_date, days, leave_type, leave_status')
			->editColumn('created_at', function($data) {
				return toDate($data->created_at);
			})
			->editColumn('leave_start_date', function($data) {
				return toDate($data->leave_start_date);
			})
			->editColumn('leave_end_date', function($data) {
				return toDate($data->leave_end_date);
			})			
			->editColumn('leave_type', function($data) {
				return toSelect(lang('_leave_type'), $data->leave_type);
			}) 
			->editColumn('leave_status', function($data) {
				return labelRequestStatus($data->leave_status);							
			})						
			->addColumn('', function($data){
				return '<a href="'.base_url(getGlobalVar('company')->slug . '/leave/detail/' . $data->leave_id).'"><span class="fa fa-search"></span></a>';							
			})								
			->render();			
			return $this->CI->output->set_content_type('application/json')->set_output($response);
		}
		$this->CI->load->view('leave/indonesia/index');
	}

	public function detail($leave_id) {
		if (!$leave = $this->model->detail($leave_id, getAuth('employee_id'))) {
			show_404();
		}

		$this->CI->load->view('leave/indonesia/detail', array(
			'leave' => $leave
		));
	}

	public function request() {
		if ($post = $this->CI->input->post()) {
			$this->CI->load->library('form_validation');
			$this->CI->form_validation->set_rules('leave_type', lang('leave_type'), 'required');
			$this->CI->form_validation->set_rules('leave_start_date', lang('leave_periode'), 'required');
			$this->CI->form_validation->set_rules('leave_end_date', lang('leave_periode'), 'required');
			$this->CI->form_validation->set_rules('description', lang('description'), 'required');
			if (!$this->CI->form_validation->run()) {
				$this->CI->redirect->withInput()->withValidation()->back();
			}
			if ($this->model->request($post['leave_type'], $post['leave_start_date'], $post['leave_end_date'], $post['description'])) {
				$this->CI->redirect->with('successMessage', lang('success_send_request_leave'))->to(getGlobalVar('company')->slug . '/leave/request');
			} else {
				$this->CI->redirect->with('errorMessage', lang('error_send_request_leave'))->back();
			}
		}
		$this->CI->load->view('leave/indonesia/request');
	}

	public function cancel($leave_id) {		
		if (!$request = $this->model->findLeave($leave_id)) {
			show_404();
		}

		if ($this->model->cancel($leave_id)) {
			$this->CI->redirect->with('successMessage', lang('success_cancel_request_leave'))->back();
		} else {
			$this->CI->redirect->with('errorMessage', lang('error_cancel_request_leave'))->back();
		}
	}	

	public function confirmation($leave_id) {
		if (!$leave = $this->model->confirmation($leave_id)) {
			show_404();
		}

		$this->CI->load->view('leave/indonesia/confirmation', array(
			'leave' => $leave
		));
	}

	public function approve($leave_id) {
		if (!$leave = $this->model->confirmation($leave_id)) {
			show_404();
		}

		if ($this->model->approve($leave_id)) {
			$this->CI->redirect->with('successMessage', lang('success_approve_request_leave'))->back();
		} else {
			$this->CI->redirect->with('errorMessage', lang('error_approve_request_leave'))->back();
		}
	}

	public function reject($leave_id) {
		if (!$leave = $this->model->confirmation($leave_id)) {
			show_404();
		}

		if ($this->model->reject($leave_id)) {
			$this->CI->redirect->with('successMessage', lang('success_reject_request_leave'))->back();
		} else {
			$this->CI->redirect->with('errorMessage', lang('error_reject_request_leave'))->back();
		}
	}

	public function suspend($leave_id) {
		if (!$leave = $this->model->confirmation($leave_id)) {
			show_404();
		}

		if ($this->model->suspend($leave_id)) {
			$this->CI->redirect->with('successMessage', lang('success_suspend_request_leave'))->back();
		} else {
			$this->CI->redirect->with('errorMessage', lang('error_suspend_request_leave'))->back();
		}
	}

}