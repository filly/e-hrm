<?php section('content') ?>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
    <div class="row">
        <div class="col-md-9">
            <?= $this->form->open('', 'class="form-horizontal"') ?>
            <h3 class="form-title"><?= lang('identity') ?></h3>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('relationship') ?></label>
                <div class="col-md-5">
                    <?= $this->form->select('relationship', lang('_relationship'), null,'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('name') ?></label>
                <div class="col-md-9">
                    <?= $this->form->text('name', null, 'placeholder="'.lang('enter_name').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('born') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('born_place', null, 'placeholder="'.lang('enter_born_place').'" class="form-control"') ?>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <?= $this->form->date('born_date', null, 'placeholder="'.lang('enter_born_date').'" class="form-control datepicker"') ?>
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('profession') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('occupation', null, 'placeholder="'.lang('enter_profession').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('workplace') ?></label>
                <div class="col-md-9">
                    <?= $this->form->text('workplace', null, 'placeholder="'.lang('enter_workplace').'" class="form-control"') ?>
                </div>
            </div>
            <h3 class="form-title"><?= lang('married') ?></h3>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('marital_status') ?></label>
                <div class="col-md-5">
                    <?= $this->form->select('marital_status', lang('_marital_status'), null,'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('married_date') ?></label>
                <div class="col-md-4">
                    <div class="input-group">
                        <?= $this->form->date('married_date', null, 'placeholder="'.lang('enter_married_date').'" class="form-control datepicker"') ?>
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="form-title"><?= lang('addresses') ?></h3>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('description') ?></label>
                <div class="col-md-6">
                    <?= $this->form->text('address_description', null, 'placeholder="'.lang('enter_description').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('address') ?></label>
                <div class="col-md-6">
                    <?= $this->form->textarea('address', null, 'placeholder="'.lang('enter_address').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('city') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('city', null, 'placeholder="'.lang('enter_city').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('phone') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('phone', null, 'placeholder="'.lang('enter_phone').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('zip_code') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('zip_code', null, 'placeholder="'.lang('enter_zip_code').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('home_status') ?></label>
                <div class="col-md-4">
                    <?= $this->form->select('home_status', lang('_home_status'), null,'class="form-control"') ?>
                </div>
            </div>
            <h3 class="form-title"><?= lang('contacts') ?></h3>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('media') ?></label>
                <div class="col-md-4">
                    <?= $this->form->select('media', lang('_media'), null,'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('contact_type') ?></label>
                <div class="col-md-4">
                    <?= $this->form->select('contact_type', lang('_contact_type'), null,'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('description') ?></label>
                <div class="col-md-6">
                    <?= $this->form->text('contact_description', null, 'placeholder="'.lang('enter_description').'" class="form-control"') ?>
                </div>
            </div>
            <h3 class="form-title"><?= lang('educations') ?></h3>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('grade') ?></label>
                <div class="col-md-6">
                    <?= $this->form->select('grade', lang('_education'), null, 'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('major') ?></label>
                <div class="col-md-6">
                    <?= $this->form->text('major', null, 'placeholder="'.lang('enter_major').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('institution') ?></label>
                <div class="col-md-6">
                    <?= $this->form->text('institution', null, 'placeholder="'.lang('enter_institution').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('graduated') ?></label>
                <div class="col-md-4">
                    <?= $this->form->select('graduated', lang('_boolean'), null,'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('certificate_id') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('certificate_id', null, 'placeholder="'.lang('enter_certificate_id').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('graduation_date') ?></label>
                <div class="col-md-4">
                    <div class="input-group">
                        <?= $this->form->date('graduation_date', null, 'placeholder="'.lang('enter_graduation_date').'" class="form-control datepicker"') ?>
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?= lang('result') ?></label>
                <div class="col-md-4">
                    <?= $this->form->text('result', null, 'placeholder="'.lang('enter_result').'" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-3 col-md-9">
                    <?= $this->form->submit('btn_save_changes', lang('save'), 'class="btn btn-success"') ?>
                    <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"') ?>
                </div>
            </div>
            <?= $this->form->close() ?>
        </div>
    </div>
<?php endsection() ?>

<?php getview('personal_data') ?><?php
/**
 * Created by PhpStorm.
 * User: Av
 * Date: 29-Feb-16
 * Time: 6:53 AM
 */