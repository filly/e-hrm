<?php section('content') ?>	
	<h3 class="form-title">
		<?= lang('families') ?>
		<?= anchor(getGlobalVar('company')->slug . '/families/add', lang('add'), 'class="btn btn-default pull-right"') ?>
	</h3>		
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>	
	<?php foreach ($families->result() as $family) { ?>
		<div class="data-list-item data-list-item-hover">
			<div class="row">
				<div class="col-md-4">
					<b><?= $family->name ?></b>
					<label class="label label-info"><?= toSelect(lang('_relationship'), $family->relationship) ?></label><br />
					<?php
						$born = Carbon\Carbon::createFromDate(
							date('Y', strtotime($family->born_date)),
							date('m', strtotime($family->born_date)),
							date('d', strtotime($family->born_date))
						);							
					?>
					(<?= lang('age') ?> : <?= $born->age ?> <?= lang('years_old') ?>)
					<div class="profile-list">
						<?= lang('born') ?>
						<span class="pull-right"><?= $family->born_place ?>, <?= humanDate($family->born_date) ?></span>
					</div>
					<div class="profile-list">
						<?= lang('married') ?>
						<span class="pull-right">
							<?php if ($family->marital_status == 'true') { ?>
								<?= lang('since') ?> <?= humanDate($family->married_date) ?>
							<?php } else { ?>
								<?= lang('not_married') ?>
							<?php } ?>
						</span>
					</div>	
					<div class="btn-group">	
						<?= anchor(getGlobalVar('company')->slug . '/families/edit/' . $family->employee_family_id, '<span class="glyphicon glyphicon-edit"></span> ' . lang('edit'), 'class="btn btn-warning btn-xs"') ?>
						<?= anchor(getGlobalVar('company')->slug . '/families/remove/' . $family->employee_family_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
					</div>
				</div>				
				<div class="col-md-4">
					<b><?= lang('address') ?></b>				
					<?php $addresses = $this->employees_model->getEmployeeAddresses(getAuth('employee_id'), $family->employee_family_id) ?>
					<?php foreach ($addresses->result() as $address) { ?>
						<p>
							<?= $address->description ?><br />
							<?= $address->address ?>,
							<?= $address->city ?>
							<?php if ($address->zip_code) { ?>
								, <?= lang('zip_code') ?> : <?= $address->zip_code ?>
							<?php } ?>
							<?php if ($address->phone) { ?>
								, <?= lang('phone') ?> : <?= $address->phone ?>
							<?php } ?>
						</p>
					<?php } ?>	
					<b><?= lang('contacts') ?></b>					
					<?php $phones = $this->employees_model->getEmployeePhones(getAuth('employee_id'), $family->employee_family_id) ?>
					<p>
					<?php foreach ($phones->result() as $phone) { ?>
						<span class="fa fa-phone fa-fw"></span> <?= $phone->description ?> (<?= toSelect(lang('_contact_type'), $phone->contact_type) ?>)
					<?php } ?>
					</p>
					<?php $mobiles = $this->employees_model->getEmployeeMobiles(getAuth('employee_id'), $family->employee_family_id) ?>
					<p>
					<?php foreach ($mobiles->result() as $mobile) { ?>
						<span class="fa fa-mobile fa-fw"></span> <?= $mobile->description ?> (<?= toSelect(lang('_contact_type'), $mobile->contact_type) ?>)
					<?php } ?>
					</p>
					<?php $emails = $this->employees_model->getEmployeeEmails(getAuth('employee_id'), $family->employee_family_id) ?>
					<p>
					<?php foreach ($emails->result() as $email) { ?>
						<span class="fa fa-envelope fa-fw"></span> <?= $email->description ?> (<?= toSelect(lang('_contact_type'), $email->contact_type) ?>)
					<?php } ?>
					</p>
				</div>
				<div class="col-md-4">										
					<p>
						<b><?= lang('profession') ?></b><br />												
						<?= $family->occupation ?>										
					</p>
					<p>
						<b><?= lang('educations') ?></b><br />
						<?php $educations = $this->employees_model->getEmployeeEducations(getAuth('employee_id'), $family->employee_family_id) ?>
						<?php foreach ($educations->result() as $education) { ?>							
								<?= toSelect(lang('_education'), $education->grade) ?> -
								<?= $education->institution ?>							
								<?php if ($education->major) { ?>
									(<?= $education->major ?>)
								<?php } ?><br />
								<?php if ($education->graduated == 'true') { ?>
									<?= lang('graduated') ?> : <?= humanDate($education->graduation_date) ?>  (<?=lang('result') ?> : <?= $education->result ?>)
								<?php } else { ?>
									<?= lang('still_in_education') ?>
								<?php } ?>
						<?php } ?>
					</p>
				</div>
			</div>
		</div>
	<?php } ?>
	<h3 class="form-title"><?= lang('waiting_approval') ?></h3>
	<?php foreach ($requests->result() as $request) { ?>
		<?php $family = json_decode($request->request_data) ?>
		<div class="data-list-item data-list-item-hover">
			<div class="row">
				<div class="col-md-4">
					<b><?= $family->name ?></b>
					<label class="label label-info"><?= toSelect(lang('_relationship'), $family->relationship) ?></label><br />
					<?php
					$born = Carbon\Carbon::createFromDate(
						date('Y', strtotime($family->born_date)),
						date('m', strtotime($family->born_date)),
						date('d', strtotime($family->born_date))
					);
					?>
					(<?= lang('age') ?> : <?= $born->age ?> <?= lang('years_old') ?>)
					<div class="profile-list">
						<?= lang('born') ?>
						<span class="pull-right"><?= $family->born_place ?>, <?= humanDate($family->born_date) ?></span>
					</div>
					<div class="profile-list">
						<?= lang('married') ?>
						<span class="pull-right">
								<?php if ($family->marital_status == 'true') { ?>
									<?= lang('since') ?> <?= humanDate($family->married_date) ?>
								<?php } else { ?>
									<?= lang('not_married') ?>
								<?php } ?>
							</span>
					</div>
					<div class="btn-group">
						<?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
					</div>
				</div>
				<div class="col-md-4">
					<b><?= lang('address') ?></b>
					<p>
						<?= $family->address_description ?><br />
						<?= $family->address ?>,
						<?= $family->city ?>
						<?php if ($family->zip_code) { ?>
							, <?= lang('zip_code') ?> : <?= $family->zip_code ?>
						<?php } ?>
						<?php if ($family->phone) { ?>
							, <?= lang('phone') ?> : <?= $family->phone ?>
						<?php } ?>
					</p>
					<b><?= lang('contacts') ?></b>
					<p>
						<?php if($family->media == 'phone'){ ?>
							<span class="fa fa-phone fa-fw"></span> <?= $family->contact_description ?> (<?= toSelect(lang('_contact_type'), $family->contact_type) ?>)
						<?php }else if($family->media == 'mobile'){ ?>
							<span class="fa fa-mobile fa-fw"></span> <?= $mobile->description ?> (<?= toSelect(lang('_contact_type'), $family->contact_type) ?>)
						<?php }else if($family->media == 'email'){ ?>
							<span class="fa fa-envelope fa-fw"></span> <?= $email->description ?> (<?= toSelect(lang('_contact_type'), $family->contact_type) ?>)
						<?php } ?>
					</p>
				</div>
				<div class="col-md-4">
					<p>
						<b><?= lang('profession') ?></b><br />
						<?= $family->occupation ?>
					</p>
					<p>
						<b><?= lang('educations') ?></b><br />
						<?= toSelect(lang('_education'), $family->grade) ?> -
						<?= $family->institution ?>
						<?php if ($family->major) { ?>
							(<?= $family->major ?>)
						<?php } ?><br />
						<?php if ($family->graduated == 'true') { ?>
							<?= lang('graduated') ?> : <?= humanDate($family->graduation_date) ?>  (<?=lang('result') ?> : <?= $family->result ?>)
						<?php } else { ?>
							<?= lang('still_in_education') ?>
						<?php } ?>
					</p>
				</div>
			</div>
		</div>
	<?php } ?>
<?php endsection() ?>
<?php getview('personal_data') ?>