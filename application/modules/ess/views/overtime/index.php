<?php section('css') ?>
	<link href="<?= base_url('public/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
<?php endsection() ?>

<?php section('script') ?>
<script src="<?= base_url('public/plugins/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?= base_url('public/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
<script src="<?= base_url('public/dist/js/ess.overtime.js') ?>"></script>
<?php endsection() ?>

<?php section('content') ?>
<h3 class="form-title">
	<?= lang('my_request') ?>
	<?= anchor(getGlobalVar('company')->slug . '/overtime/request', lang('send_request'), 'class="btn btn-default pull-right"') ?>
</h3>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<div class="form-filter">
	<div class="row">
		<div class="col-md-4">	
			<?= $this->form->select('filter[request_status]', lang('_request_status'), null,'class="form-control"') ?>                        		
		</div>		
	</div>		
</div>
<table id="dataTables" class="table table-hover">
	<thead>
		<tr>
			<td><?= lang('created_at') ?></td>
			<td><?= lang('overtime_date') ?></td>
			<td><?= lang('start_overtime') ?></td>
			<td><?= lang('end_overtime') ?></td>
			<td><?= lang('reason') ?></td>
			<td><?= lang('description') ?></td>			
			<td class="text-center"><?= lang('request_status') ?></td>
			<td></td>
		</tr>
	</thead>
</table>
<?php endsection() ?>	

<?php getview('overtime') ?>	