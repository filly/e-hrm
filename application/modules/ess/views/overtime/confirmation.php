<?php getview('template/header') ?>
<div class="box box-primary box-sm">
	<div class="box-header with-border">
		<h1 class="box-title">
			<?= lang('request_overtime') ?>					
		</h1>
		<?= anchor(getGlobalVar('company')->slug . '/request', lang('back'), 'class="btn btn-default pull-right"') ?>	
	</div>	
	<div class="box-body">						
		<?php getview('template/partials/message') ?>
		<?php getview('template/partials/validation') ?>
		<h3 class="form-title">
			<?= lang('detail') ?>
			<span class="pull-right"><?= labelRequestStatus($overtime->overtime_status) ?></span>
		</h3>
		<table class="table-info">
			<tr>
				<td width="200"><?= lang('name') ?></td>
				<td class="text-right"><?= $overtime->first_name ?> <?= $overtime->last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('employee_code') ?></td>
				<td class="text-right"><?= $overtime->employee_code ?></td>
			</tr>			
			<tr>
				<td><?= lang('permit_date') ?></td>
				<td class="text-right"><?= humanDate($overtime->overtime_date) ?></td>
			</tr>
			<tr>
				<td><?= lang('time_overtime') ?></td>
				<td class="text-right">
					<?= toTime($overtime->start_overtime) ?>
					<?= lang('_to') ?>
					<?= toTime($overtime->end_overtime) ?>
				</td>
			</tr>					
			<?php if ($overtime->confirmed_by) { ?>
			<tr>
				<td><?= lang('confirmed_by') ?></td>
				<td class="text-right"><?= $overtime->confirmed_by_first_name ?> <?= $overtime->confirmed_by_last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('confirmed_at') ?></td>
				<td class="text-right"><?= humanDateTime($overtime->confirmed_at) ?></td>
			</tr>
			<?php } ?>			
		</table>	
		<h3 class="form-title">		
			<?= lang('employees') ?>
		</h3>
		<table class="table-info">
			<?php foreach ($employees->result() as $employee) { ?>
				<tr>
					<td><?= $employee->employee_code ?></td>
					<td class="text-right"><?= $employee->first_name ?> <?= $employee->last_name ?></td>
				</tr>
			<?php } ?>	
		</table>
		<?php if ($overtime->overtime_status == 'waiting') { ?>
			<?= anchor(getGlobalVar('company')->slug . '/overtime/approve/' . $overtime->overtime_id, lang('approve'), 'class="btn btn-primary pull-left"') ?>			
			<?= anchor(getGlobalVar('company')->slug . '/overtime/reject/' . $overtime->overtime_id, lang('reject'), 'class="btn btn-danger pull-right"') ?>
		<?php } else { ?>		
			<?= anchor(getGlobalVar('company')->slug . '/overtime/suspend/' . $overtime->overtime_id, lang('suspend'), 'class="btn btn-warning pull-right"') ?>
		<?php } ?>
	</div>
</div>
<?php getview('template/footer') ?>