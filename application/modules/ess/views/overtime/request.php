<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>
	<?= $this->form->open('', 'class="form-horizontal"') ?>
	<h3 class="form-title">
		<?= lang('send_request') ?>
		<?= anchor(getGlobalVar('company')->slug . '/overtime', lang('my_request'), 'class="btn btn-default pull-right"') ?>
	</h3>
	<div class="form-group">
	  	<label class="col-md-3 control-label"><?= lang('overtime_date') ?></label>
	  	<div class="col-md-5">
	  		<div class="input-group">
	      		<div class="input-group-addon">
	      			<span class="fa fa-calendar"></span>
	      		</div>
	      		<?= $this->form->date('overtime_date', null, 'placeholder="'.lang('enter_overtime_date').'" class="form-control datepicker"') ?>                        
	      	</div>
	  	</div>
		</div>		
		<div class="form-group">
	 	<label class="col-md-3 control-label"><?= lang('time_overtime') ?></label>
	  	<div class="col-md-5">
	  		<div class="input-group">
	        	<?= $this->form->time('start_overtime', null, 'placeholder="'.lang('enter_start_overtime').'" class="form-control timepicker"') ?>                        
	        	<div class="input-group-addon">
	        		<?= lang('_to') ?>
	        	</div>
	        	<?= $this->form->time('end_overtime', null, 'placeholder="'.lang('enter_end_overtime').'" class="form-control timepicker"') ?>                        
	    	</div>
	  	</div>
		</div>		  
		<div class="form-group">
	    <label class="col-md-3 control-label"><?= lang('reason') ?></label>
	    <div class="col-md-5">
	        <?= $this->form->textarea('reason', null, 'placeholder="'.lang('enter_reason').'" class="form-control"') ?>                        
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-md-3 control-label"><?= lang('description') ?></label>
	    <div class="col-md-5">
	        <?= $this->form->textarea('description', null, 'placeholder="'.lang('enter_description').'" class="form-control"') ?>                        
	    </div>
	</div>		  	
	<div class="form-group">                        
		<div class="col-md-offset-3 col-md-9">
		    <?= $this->form->submit('btn_submit', lang('send'), 'class="btn btn-success"') ?>            
		    <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"') ?>  
		</div>
	</div>		    			        			    	    			        			   		    			        			    	    			        			 
	<?= $this->form->close() ?>
<?php endsection() ?>	
<?php getview('overtime') ?>	