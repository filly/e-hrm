<?php getview('template/header') ?>
<div class="box box-primary box-sm">
	<div class="box-header with-border">
		<h1 class="box-title">
			<?= lang('request_training') ?>		
		</h1>
	</div>	
	<div class="box-body">						
		<?php getview('template/partials/message') ?>
		<?php getview('template/partials/validation') ?>
		<h3 class="form-title">
			<?= lang('detail') ?>
			<span class="pull-right"><?= labelRequestStatus($training->training_status) ?></span>
		</h3>
		<table class="table-info">
			<tr>
				<td width="200"><?= lang('name') ?></td>
				<td class="text-right"><?= $training->first_name ?> <?= $training->last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('employee_code') ?></td>
				<td class="text-right"><?= $training->employee_code ?></td>
			</tr>	
			<tr>
				<td><?= lang('training') ?></td>
				<td class="text-right"><?= $training->training ?></td>
			</tr>	
			<tr>
				<td><?= lang('training_start_date') ?></td>
				<td class="text-right"><?= humanDateTime($training->training_start_date) ?></td>
			</tr>
			<tr>
				<td><?= lang('training_end_date') ?></td>
				<td class="text-right"><?= humanDateTime($training->training_end_date) ?></td>
			</tr>	
			<tr>
				<td><?= lang('cost') ?></td>
				<td class="text-right"><?= toCurrency($training->cost) ?></td>
			</tr>	
			<tr>
				<td><?= lang('place') ?></td>
				<td class="text-right"><?= $training->place ?></td>
			</tr>	
			<tr>
				<td><?= lang('trainer') ?></td>
				<td class="text-right"><?= $training->trainer ?></td>
			</tr>										
			<?php if ($training->confirmed_by) { ?>
			<tr>
				<td><?= lang('confirmed_by') ?></td>
				<td class="text-right"><?= $training->confirmed_by_first_name ?> <?= $training->confirmed_by_last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('confirmed_at') ?></td>
				<td class="text-right"><?= humanDateTime($training->confirmed_at) ?></td>
			</tr>
			<?php } ?>			
		</table>	
		<h3 class="form-title">		
			<?= lang('employees') ?>
		</h3>
		<table class="table-info">
			<?php foreach ($employees->result() as $employee) { ?>
				<tr>
					<td><?= $employee->employee_code ?></td>
					<td class="text-right"><?= $employee->first_name ?> <?= $employee->last_name ?></td>
				</tr>
			<?php } ?>	
		</table>
		<?= anchor(getGlobalVar('company')->slug . '/training', lang('back'), 'class="btn btn-default"') ?>
		<?php if ($training->training_status == 'waiting') { ?>
			<?= anchor(getGlobalVar('company')->slug . '/training/cancel/' . $training->training_id, lang('cancel'), 'class="btn btn-danger pull-right"') ?>
		<?php } ?>
	</div>
</div>
<?php getview('template/footer') ?>