<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>
	<?= $this->form->open('', 'class="form-horizontal"') ?>
	<h3 class="form-title">
		<?= lang('send_request') ?>
		<?= anchor(getGlobalVar('company')->slug . '/training', lang('my_request'), 'class="btn btn-default pull-right"') ?>
	</h3>
	<div class="form-group">
		<label class="col-md-3 control-label"><?= lang('training') ?></label>
		<div class="col-md-5">
			<?= $this->form->text('training', null, 'placeholder="'.lang('enter_training').'" class="form-control"') ?>
		</div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"><?= lang('training_date') ?></label>
	  <div class="col-md-7">
	  	<div class="input-group periode-datetimepicker">
		    <?= $this->form->datetime('training_start_date', null, 'placeholder="'.lang('enter_training_start_date').'" class="form-control datetimepicker datetimepicker-start"') ?>
		    	<div class="input-group-addon">
		    		<?= lang('_to') ?>
		    	</div>
		    <?= $this->form->datetime('training_end_date', null, 'placeholder="'.lang('enter_training_end_date').'" class="form-control datetimepicker datetimepicker-end"') ?>
	    </div>
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"><?= lang('cost') ?></label>
	  <div class="col-md-5">
	    <?= $this->form->text('cost', null, 'placeholder="'.lang('enter_cost').'" class="form-control text-right"') ?>
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"><?= lang('place') ?></label>
	  <div class="col-md-5">
	    <?= $this->form->text('place', null, 'placeholder="'.lang('enter_place').'" class="form-control"') ?>
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"><?= lang('trainer') ?></label>
	  <div class="col-md-5">
	    <?= $this->form->text('trainer', null, 'placeholder="'.lang('enter_trainer').'" class="form-control"') ?>
	  </div>
	</div>
	<div class="form-group">                        
	      <div class="col-md-offset-3 col-md-9">
	            <?= $this->form->submit('btn_send', lang('send'), 'class="btn btn-success"') ?>            
	            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"') ?>  
	      </div>
	</div>	    			        			    	    			        			   		    			        			    	    			        			 
	<?= $this->form->close() ?>
<?php endsection() ?>	
<?php getview('training') ?>	