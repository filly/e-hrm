<?php section('css') ?>
	<link href="<?= base_url('public/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
<?php endsection() ?>

<?php section('script') ?>
<script src="<?= base_url('public/plugins/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?= base_url('public/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
<script src="<?= base_url('public/dist/js/ess.training.js') ?>"></script>
<?php endsection() ?>

<?php section('content') ?>
<h3 class="form-title">
	<?= lang('my_request') ?>
	<?= anchor(getGlobalVar('company')->slug . '/training/request', lang('send_request'), 'class="btn btn-default pull-right"') ?>
</h3>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<div class="form-filter">
	<div class="row">
		<div class="col-md-4">	
			<?= $this->form->select('filter[request_status]', lang('_request_status'), null,'class="form-control"') ?>                        		
		</div>		
	</div>		
</div>
<table id="dataTables" class="table table-hover">
	<thead>
		<tr>
			<td><?= lang('created_at') ?></td>
			<td><?= lang('training') ?></td>
			<td><?= lang('training_start_date') ?></td>
			<td><?= lang('training_end_date') ?></td>
			<td><?= lang('cost') ?></td>
			<td><?= lang('place') ?></td>			
			<td><?= lang('trainer') ?></td>			
			<td><?= lang('request_status') ?></td>	
			<td></td>
		</tr>
	</thead>
</table>
<?php endsection() ?>	

<?php getview('training') ?>	