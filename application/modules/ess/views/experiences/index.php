<?php section('content') ?>
    <h3 class="form-title">
        <?= lang('experiences') ?>
        <?= anchor(getGlobalVar('company')->slug . '/experiences/add', lang('add'), 'class="btn btn-default pull-right"') ?>
    </h3>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<?php foreach ($experiences->result() as $experience) { ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= $experience->workplace ?></b>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/experiences/edit/' . $experience->employee_experience_id, '<span class="glyphicon glyphicon-edit"></span> ' . lang('edit'), 'class="btn btn-warning btn-xs"') ?>
            <?= anchor(getGlobalVar('company')->slug . '/experiences/remove/' . $experience->employee_experience_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= $experience->occupation ?><br />
            <?= lang('start_work_date') ?> : <?= antiNull(toDate($experience->start_work_date)) ?><br />
            <?= lang('end_work_date') ?> : <?= antiNull(toDate($experience->end_work_date)) ?>
        </p>
    </div>
<?php } ?>
    <h3 class="form-title"><?= lang('waiting_approval') ?></h3>
<?php foreach ($requests->result() as $request) { ?>
    <?php $experience = json_decode($request->request_data) ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= $experience->workplace ?></b> <?= labelRequestType($request->request_type) ?>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="fa fa-times"></span> ' . lang('cancel'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= $experience->occupation ?><br />
            <?= lang('start_work_date') ?> : <?= antiNull(toDate($experience->start_work_date)) ?><br />
            <?= lang('end_work_date') ?> : <?= antiNull(toDate($experience->end_work_date)) ?>
        </p>
    </div>
<?php } ?>
<?php endsection() ?>
<?php getview('personal_data') ?>