<?php section('content') ?>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<?= $this->form->open('', 'class="form-horizontal"') ?>
    <h3 class="form-title"><?= lang('experiences') ?></h3>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('workplace') ?></label>
        <div class="col-md-6">
            <?= $this->form->text('workplace', null, 'placeholder="'.lang('enter_workplace').'" class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('occupation') ?></label>
        <div class="col-md-6">
            <?= $this->form->text('occupation', null, 'placeholder="'.lang('enter_occupation').'" class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('start_work_date') ?></label>
        <div class="col-md-4">
            <div class="input-group">
                <?= $this->form->date('start_work_date', null, 'placeholder="'.lang('enter_start_work_date').'" class="form-control datepicker"') ?>
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('end_work_date') ?></label>
        <div class="col-md-4">
            <div class="input-group">
                <?= $this->form->date('end_work_date', null, 'placeholder="'.lang('enter_end_work_date').'" class="form-control datepicker"') ?>
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= $this->form->submit('btn_save', lang('save'), 'class="btn btn-success"') ?>
            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"')  ?>
        </div>
    </div>
<?= $this->form->close() ?>
<?php endsection() ?>

<?php getview('personal_data') ?>