<?php section('css') ?>
	<link href="<?= base_url('public/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
<?php endsection() ?>

<?php section('script') ?>
<script src="<?= base_url('public/plugins/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?= base_url('public/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
<script src="<?= base_url('public/dist/js/ess.request.js') ?>"></script>
<?php endsection() ?>

<?php getview('template/header') ?>	
	<div class="box box-primary">
		<div class="box-header with-border">
			<h1 class="box-title"><?= lang('request') ?></h1>
		</div>
		<div class="box-body">
			<div class="form-filter">
				<div class="row">
					<div class="col-md-4">	
						<?= $this->form->select('filter[request_type]', lang('_request_type'), null,'class="form-control"') ?>                        		
					</div>
					<div class="col-md-4">	
						<?= $this->form->select('filter[data]', lang('_request_data'), null,'class="form-control"') ?>                        		
					</div>					
					<div class="col-md-4">	
						<?= $this->form->select('filter[request_status]', lang('_request_status'), null,'class="form-control"') ?>                        		
					</div>		
				</div>		
			</div>
			<table id="dataTables" class="table table-hover">
				<thead>
					<tr>
						<td><?= lang('employee_code') ?></td>
						<td><?= lang('name') ?></td>
						<td><?= lang('created_at') ?></td>
						<td><?= lang('end_overtime') ?></td>
						<td><?= lang('request_status') ?></td>						
						<td width="1"></td>
					</tr>
				</thead>
			</table>		
		</div>
	</div>	
<?php getview('template/footer') ?>