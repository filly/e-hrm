<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>
	<div class="row">
		<div class="col-md-9">		
			<?= $this->form->open('', 'class="form-horizontal"') ?>
			<h3 class="form-title"><?= lang('identity') ?></h3>
			<div class="form-group">
			    <label class="col-md-3 control-label"><?= lang('name') ?></label>
			    <div class="col-md-4">
			        <?= $this->form->text('first_name', null, 'placeholder="'.lang('enter_first_name').'" class="form-control"') ?>
			    </div>
			    <div class="col-md-5">
			        <?= $this->form->text('last_name', null, 'placeholder="'.lang('enter_last_name').'" class="form-control"') ?>
			    </div>
			</div>	  	
			<div class="form-group">
			    <label class="col-md-3 control-label"><?= lang('gender') ?></label>
			    <div class="col-md-3">
			        <?= $this->form->select('gender', lang('_gender'), null,'class="form-control"') ?>                        
			    </div>
			</div>
			<div class="form-group">
		  	    <label class="col-md-3 control-label"><?= lang('blood_type') ?></label>
		  	    <div class="col-md-3">
		  	        <?= $this->form->select('blood_type', lang('_blood_type'), null,'class="form-control"') ?>                        
		  	    </div>
		  	</div>    
		  	<div class="form-group">
			    <label class="col-md-3 control-label"><?= lang('born') ?></label>
			    <div class="col-md-4">
			        <?= $this->form->text('born_place', null, 'placeholder="'.lang('enter_born_place').'" class="form-control"') ?>                        
			    </div>
			    <div class="col-md-4">
			    	<div class="input-group">	    		
			    		<?= $this->form->date('born_date', null, 'placeholder="'.lang('enter_born_date').'" class="form-control datepicker"') ?>                        	    		
			    		<div class="input-group-addon">
			    			<span class="fa fa-calendar"></span>
			    		</div>
			    	</div>	        
			    </div>
			</div>		
			<div class="form-group">
			    <label class="col-md-3 control-label"><?= lang('height') ?> &amp; <?= lang('weight') ?></label>
			    <div class="col-md-3">
			        <div class="input-group">	    		
			    		<?= $this->form->text('height', null, 'placeholder="'.lang('height').'" class="form-control"') ?>                        	    		
			    		<div class="input-group-addon">
			    			<?= lang('_height_class') ?>
			    		</div>
			    	</div>
			    </div>
			    <div class="col-md-3">
			    	<div class="input-group">	    		
			    		<?= $this->form->text('weight', null, 'placeholder="'.lang('weight').'" class="form-control"') ?>                        	    		
			    		<div class="input-group-addon">
			    			<?= lang('_weight_class') ?>
			    		</div>
			    	</div>	        
			    </div>
			</div>	
			<div class="form-group">
				    <label class="col-md-3 control-label"><?= lang('religion') ?></label>
				    <div class="col-md-4">
				        <?= $this->form->select('religion', lang('_religion'), null,'class="form-control"') ?>                        
				    </div>
				</div>
			<div class="form-group">
			    <label class="col-md-3 control-label"><?= lang('nationality') ?></label>
			    <div class="col-md-4">
			        <?= $this->form->text('nationality', null, 'placeholder="'.lang('enter_nationality').'" class="form-control"') ?>                        
			    </div>
			</div>		  	
			<h3 class="form-title"><?= lang('married') ?></h3>
			<div class="form-group">
			    <label class="col-md-3 control-label"><?= lang('marital_status') ?></label>
			    <div class="col-md-5">
			        <?= $this->form->select('marital_status', lang('_marital_status'), null,'class="form-control"') ?>                        
			    </div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('married_date') ?></label>
			    <div class="col-md-4">
			    	<div class="input-group">	    		
			    		<?= $this->form->date('married_date', null, 'placeholder="'.lang('enter_married_date').'" class="form-control datepicker"') ?>                        	    		
			    		<div class="input-group-addon">
			    			<span class="fa fa-calendar"></span>
			    		</div>
			    	</div>	        
			    </div>
			</div>	
			<div class="form-group">                        
			      <div class="col-md-offset-3 col-md-9">
			            <?= $this->form->submit('btn_save_changes', lang('save_changes'), 'class="btn btn-success"') ?>            
			            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"') ?>  
			      </div>
			</div>
			<?= $this->form->close() ?>
		</div>
		<div class="col-md-3">
			<div class="form-image" data-url="<?= base_url(getGlobalVar('company')->slug . '/profile/updatephoto') ?>">
				<img src="<?= base_url('public/dist/img/noimage.jpg') ?>" />
				<?= $this->form->openMultipart(base_url(getGlobalVar('company')->slug . '/profile/updatephoto')) ?>
					<?= $this->form->upload('photo') ?>									
					<div class="btn-group form-image-panel">
						<button type="button" id="btn-browse" class="btn btn-primary btn-sm"><span class="fa fa-image"></span> Browse</button>
						<button type="submit" id="btn-upload" class="btn btn-success btn-sm"><span class="fa fa-upload"></span> Upload</button>
					</div>
				<?= $this->form->close() ?>
			</div>
		</div>	
	</div>		
<?php endsection() ?>

<?php getview('personal_data') ?>