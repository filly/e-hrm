<?php getview('template/header') ?>	
	<div class="box box-primary">
		<div class="box-header with-border">
			<h1 class="box-title"><?= lang('manage_personal_data') ?></h1>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">				
					<ul class="box-nav">							
						<li><h3><?= lang('general') ?></h3></li>
						<li><a href="<?= base_url(getGlobalVar('company')->slug . '/profile') ?>"><?= lang('profile') ?></a></li>        
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/contacts') ?>"><?= lang('contacts') ?></a></li>
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/addresses') ?>"><?= lang('addresses') ?></a></li>        
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/families') ?>"><?= lang('families') ?></a></li>        
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/educations') ?>"><?= lang('educations') ?></a></li>
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/experiences') ?>"><?= lang('experiences') ?></a></li>        
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/identity-cards') ?>"><?= lang('identity_cards') ?></a></li>
				        <li><a href="<?= base_url(getGlobalVar('company')->slug . '/documents') ?>"><?= lang('documents') ?></a></li>
					</ul>					
				</div>
				<div class="col-md-9 form-wrapper">
					<?= render('content') ?>	
				</div>
			</div>			
		</div>
	</div>	
<?php getview('template/footer') ?>