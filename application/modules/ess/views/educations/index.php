<?php section('content') ?>
    <h3 class="form-title">
        <?= lang('educations') ?>
        <?= anchor(getGlobalVar('company')->slug . '/educations/add', lang('add'), 'class="btn btn-default pull-right"') ?>
    </h3>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<?php foreach ($educations->result() as $education) { ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= toSelect(lang('_education'), $education->grade) ?> (<?= $education->institution ?>)</b>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/educations/edit/' . $education->employee_education_id, '<span class="glyphicon glyphicon-edit"></span> ' . lang('edit'), 'class="btn btn-warning btn-xs"') ?>
            <?= anchor(getGlobalVar('company')->slug . '/educations/remove/' . $education->employee_education_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= $education->major ?><br />
            <?= lang('graduation_date') ?> : <?= antiNull(toDate($education->graduation_date)) ?>
        </p>
    </div>
<?php } ?>
    <h3 class="form-title"><?= lang('waiting_approval') ?></h3>
<?php foreach ($requests->result() as $request) { ?>
    <?php $education = json_decode($request->request_data) ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= toSelect(lang('_education'), $education->grade) ?> (<?= $education->institution ?>)</b> <?= labelRequestType($request->request_type) ?>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="fa fa-times"></span> ' . lang('cancel'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= $education->major ?><br />
            <?= lang('graduation_date') ?> : <?= antiNull(toDate($education->graduation_date)) ?>
        </p>
    </div>
<?php } ?>
<?php endsection() ?>
<?php getview('personal_data') ?>