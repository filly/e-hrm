<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>	
		<?= $this->form->open('', 'class="form-horizontal"') ?>
		<h3 class="form-title"><?= lang('educations') ?></h3>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('grade') ?></label>
		    <div class="col-md-6">			      
		      <?= $this->form->select('grade', lang('_education'), null, 'class="form-control"') ?>
		    </div>
		</div>			  
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('major') ?></label>
		    <div class="col-md-6">
		        <?= $this->form->text('major', null, 'placeholder="'.lang('enter_major').'" class="form-control"') ?>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('institution') ?></label>
		    <div class="col-md-6">
		        <?= $this->form->text('institution', null, 'placeholder="'.lang('enter_institution').'" class="form-control"') ?>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('certificate_id') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->text('certificate_id', null, 'placeholder="'.lang('enter_certificate_id').'" class="form-control"') ?>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('graduated') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->select('graduated', lang('_boolean'), null,'class="form-control"') ?>
		    </div>
		</div>
		<div class="form-group">
            <label class="col-md-3 control-label"><?= lang('graduation_date') ?></label>
            <div class="col-md-4">
                <div class="input-group">
                    <?= $this->form->date('graduation_date', null, 'placeholder="'.lang('enter_graduation_date').'" class="form-control datepicker"') ?>
                    <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label"><?= lang('result') ?></label>
            <div class="col-md-4">
                <?= $this->form->text('result', null, 'placeholder="'.lang('enter_result').'" class="form-control"') ?>
            </div>
        </div>
		<div class="form-group">                        
		      <div class="col-md-offset-3 col-md-9">
		            <?= $this->form->submit('btn_save', lang('save'), 'class="btn btn-success"') ?>           
		            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"')  ?>
		      </div>
		</div>
		<?= $this->form->close() ?>
<?php endsection() ?>

<?php getview('personal_data') ?>