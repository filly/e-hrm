<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>
	<?= $this->form->open('', 'class="form-horizontal"') ?>
	<h3 class="form-title">
		<?= lang('send_request') ?>
		<?= anchor(getGlobalVar('company')->slug . '/leave', lang('my_request'), 'class="btn btn-default pull-right"') ?>
	</h3>
	<div class="form-group">
	    <label class="col-md-3 control-label"><?= lang('leave_type') ?></label>
	    <div class="col-md-3">
	        <?= $this->form->select('leave_type', lang('_leave_type'), null,'class="form-control"') ?>                        
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-md-3 control-label"><?= lang('leave_periode') ?></label>
	    <div class="col-md-5">			    	
	    	<div class="input-group periode-datepicker">			    				    	
	        	<?= $this->form->text('leave_start_date', null, 'placeholder="'.lang('enter_start_date').'" class="form-control date"') ?>
	        	<div class="input-group-addon">                        
	        		<?= lang('_to') ?>
	        	</div>
	        	<?= $this->form->text('leave_end_date', null, 'placeholder="'.lang('enter_end_date').'" class="form-control date"') ?>                        
	        </div>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-md-3 control-label"><?= lang('description') ?></label>
	    <div class="col-md-5">
	        <?= $this->form->textarea('description', null, 'placeholder="'.lang('enter_description').'" class="form-control"') ?>                        
	    </div>
	</div>	
	<div class="form-group">                        
		<div class="col-md-offset-3 col-md-9">
            <?= $this->form->submit('btn_submit', lang('send'), 'class="btn btn-success"') ?>            
            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"') ?>  
        </div>
  	</div>	
<?php endsection() ?>
<?php getview('leave/indonesia/indonesia') ?>