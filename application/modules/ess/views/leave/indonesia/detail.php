<?php getview('template/header') ?>
<div class="box box-primary box-sm">
	<div class="box-header with-border">
		<h1 class="box-title">
			<?= lang('request_overtime') ?>		
		</h1>
	</div>	
	<div class="box-body">						
			<?php getview('template/partials/message') ?>
		<?php getview('template/partials/validation') ?>
		<h3 class="form-title">
			<?= lang('detail') ?>
			<span class="pull-right"><?= labelRequestStatus($leave->leave_status) ?></span>
		</h3>
		<table class="table-info">
			<tr>
				<td width="200"><?= lang('name') ?></td>
				<td class="text-right"><?= $leave->first_name ?> <?= $leave->last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('employee_code') ?></td>
				<td class="text-right"><?= $leave->employee_code ?></td>
			</tr>			
			<tr>
				<td><?= lang('leave_type') ?></td>
				<td class="text-right"><?= toSelect(lang('_leave_type'), $leave->leave_type) ?></td>
			</tr>
			<tr>
				<td><?= lang('leave_start_date') ?></td>
				<td class="text-right"><?= humanDate($leave->leave_start_date) ?></td>
			</tr>
			<tr>
				<td><?= lang('leave_end_date') ?></td>
				<td class="text-right"><?= humanDate($leave->leave_end_date) ?></td>
			</tr>
			<tr>
				<td><?= lang('num_of_days') ?></td>
				<td class="text-right"><?= $leave->days ?> <?= lang('days') ?></td>
			</tr>		
			<tr>
				<td><?= lang('description') ?></td>
				<td class="text-right"><?= $leave->description ?></td>
			</tr>						
			<?php if ($leave->confirmed_by) { ?>
			<tr>
				<td><?= lang('confirmed_by') ?></td>
				<td class="text-right"><?= $leave->confirmed_by_first_name ?> <?= $leave->confirmed_by_last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('confirmed_at') ?></td>
				<td class="text-right"><?= humanDateTime($leave->confirmed_at) ?></td>
			</tr>
			<?php } ?>			
		</table>					
		<?= anchor(getGlobalVar('company')->slug . '/leave', lang('back'), 'class="btn btn-default"') ?>
		<?php if ($leave->leave_status == 'waiting') { ?>
			<?= anchor(getGlobalVar('company')->slug . '/leave/cancel/' . $leave->leave_id, lang('cancel'), 'class="btn btn-danger pull-right"') ?>
		<?php } ?>		
	</div>
</div>
<?php getview('template/footer') ?>