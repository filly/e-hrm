<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>	
	<h3 class="form-title"><?= lang('contacts') ?></h3>
	<table class="table table-hover">
		<?= $this->form->open() ?>
		<thead>
			<tr>
				<td width="150">
					<?= $this->form->select('media', lang('_media'), null, 'class="form-control"') ?>
				</td>
				<td width="150">
					<?= $this->form->select('contact_type', lang('_contact_type'), null, 'class="form-control"') ?>
				</td>
				<td>
					<?= $this->form->text('description', null, 'placeholder="'.lang('enter_contact').'" class="form-control"') ?>
				</td>
				<td width="1" char="text-right">
					<?= $this->form->submit('add', lang('add'), 'class="btn btn-default"') ?>
				</td>
			</tr>			
		</thead>
		<?= $this->form->close() ?>
		<tbody>
			<?php foreach ($contacts->result() as $contact) { ?>
				<tr>
					<td><?= toSelect(lang('_media'), $contact->media) ?></td>
					<td><?= toSelect(lang('_contact_type'), $contact->contact_type) ?></td>
					<td><?= $contact->description ?></td>
					<td class="text-right">
						<?= anchor(getGlobalVar('company')->slug . '/contacts/remove/' . $contact->employee_contact_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<h3 class="form-title"><?= lang('waiting_approval') ?></h3>
	<table class="table table-hover">		
		<tbody>
			<?php foreach ($requests->result() as $request) { ?>
				<?php $contact = json_decode($request->request_data) ?>
				<tr>
					<td width="150"><?= toSelect(lang('_media'), $contact->media) ?></td>
					<td width="150"><?= toSelect(lang('_contact_type'), $contact->contact_type) ?></td>
					<td>
						<?= $contact->description ?>
						<?= labelRequestType($request->request_type) ?>
					</td>					
					<td width="1" class="text-right">						
						<?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="fa fa-times"></span> ' . lang('cancel'), 'class="btn btn-danger btn-xs"') ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
<?php endsection() ?>

<?php getview('personal_data') ?>