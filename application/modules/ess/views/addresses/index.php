<?php section('content') ?>	
	<h3 class="form-title">
		<?= lang('addresses') ?>
		<?= anchor(getGlobalVar('company')->slug . '/addresses/add', lang('add'), 'class="btn btn-default pull-right"') ?>
	</h3>		
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>
	<?php foreach ($addresses->result() as $address) { ?>
		<div class="data-list-item data-list-item-hover">
			<b><?= $address->description ?> (<?= toSelect(lang('_home_status'), $address->home_status) ?>)</b>
			<div class="btn-group pull-right">
				<?= anchor(getGlobalVar('company')->slug . '/addresses/edit/' . $address->employee_address_id, '<span class="glyphicon glyphicon-edit"></span> ' . lang('edit'), 'class="btn btn-warning btn-xs"') ?>
				<?= anchor(getGlobalVar('company')->slug . '/addresses/remove/' . $address->employee_address_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
			</div>
			<br />
			<p>
				<?= $address->address ?><br />
				<?= $address->city ?> - <?= $address->zip_code ?><br />
				<?= lang('phone') ?> : <?= antiNull($address->phone) ?>
			</p>			
		</div>
	<?php } ?>
	<h3 class="form-title"><?= lang('waiting_approval') ?></h3>
	<?php foreach ($requests->result() as $request) { ?>
		<?php $address = json_decode($request->request_data) ?>
		<div class="data-list-item data-list-item-hover">
			<b><?= $address->description ?> (<?= toSelect(lang('_home_status'), $address->home_status) ?>)</b> <?= labelRequestType($request->request_type) ?>
			<div class="btn-group pull-right">				
				<?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="fa fa-times"></span> ' . lang('cancel'), 'class="btn btn-danger btn-xs"') ?>
			</div>
			<br />
			<p>
				<?= $address->address ?><br />
				<?= $address->city ?> - <?= $address->zip_code ?><br />
				<?= lang('phone') ?> : <?= antiNull($address->phone) ?>
			</p>			
		</div>
	<?php } ?>
<?php endsection() ?>
<?php getview('personal_data') ?>