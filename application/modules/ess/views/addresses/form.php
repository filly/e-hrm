<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>	
		<?= $this->form->open('', 'class="form-horizontal"') ?>
		<h3 class="form-title"><?= lang('addresses') ?></h3>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('description') ?></label>
		    <div class="col-md-6">			      
		      <?= $this->form->text('description', null, 'placeholder="'.lang('enter_description').'" class="form-control"') ?>                        
		    </div>
		</div>			  
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('address') ?></label>
		    <div class="col-md-6">
		        <?= $this->form->textarea('address', null, 'placeholder="'.lang('enter_address').'" class="form-control"') ?>                        
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('city') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->text('city', null, 'placeholder="'.lang('enter_city').'" class="form-control"') ?>                        
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('phone') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->text('phone', null, 'placeholder="'.lang('enter_phone').'" class="form-control"') ?>                        
		    </div>
		</div>
		  
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('zip_code') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->text('zip_code', null, 'placeholder="'.lang('enter_zip_code').'" class="form-control"') ?>                        
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('home_status') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->select('home_status', lang('_home_status'), null,'class="form-control"') ?>                        
		    </div>
		</div>
		<div class="form-group">                        
		      <div class="col-md-offset-3 col-md-9">
		            <?= $this->form->submit('btn_save', lang('save'), 'class="btn btn-success"') ?>           
		            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"')  ?>
		      </div>
		</div>
		<?= $this->form->close() ?>
<?php endsection() ?>

<?php getview('personal_data') ?>