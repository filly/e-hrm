<?php section('content') ?>
    <h3 class="form-title">
        <?= lang('documents') ?>
        <?= anchor(getGlobalVar('company')->slug . '/documents/add', lang('add'), 'class="btn btn-default pull-right"') ?>
    </h3>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<?php foreach ($documents->result() as $document) { ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= $document->document ?></b>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/documents/download/' . $document->document_file, '<span class="glyphicon glyphicon-download"></span> ' . lang('download'), 'class="btn btn-primary btn-xs"') ?>
            <?= anchor(getGlobalVar('company')->slug . '/documents/remove/' . $document->employee_document_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= lang('document_id') ?> : <?= $document->document_id ?><br />
            <?= $document->description ?><br />
        </p>
    </div>
<?php } ?>
    <h3 class="form-title"><?= lang('waiting_approval') ?></h3>
<?php foreach ($requests->result() as $request) { ?>
    <?php $document = json_decode($request->request_data) ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= lang('document'), $document->document ?></b> <?= labelRequestType($request->request_type) ?>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="fa fa-times"></span> ' . lang('cancel'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= lang('document_id') ?> : <?= $document->document_id ?><br />
            <?= $document->description ?><br />
        </p>
    </div>
<?php } ?>
<?php endsection() ?>
<?php getview('personal_data') ?>