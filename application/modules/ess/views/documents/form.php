<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>	
		<?= $this->form->openMultipart('', 'class="form-horizontal"') ?>
		<h3 class="form-title"><?= lang('documents') ?></h3>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('document') ?></label>
		    <div class="col-md-6">			      
		      <?= $this->form->text('document', null, 'placeholder="'.lang('enter_document').'" class="form-control"') ?>
		    </div>
		</div>			  
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('document_id') ?></label>
		    <div class="col-md-6">
		        <?= $this->form->text('document_id', null, 'placeholder="'.lang('enter_document_id').'" class="form-control"') ?>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-md-3 control-label"><?= lang('document_file') ?></label>
		    <div class="col-md-4">
		        <?= $this->form->upload('document_file') ?>
		    </div>
		</div>
		<div class="form-group">                        
		      <div class="col-md-offset-3 col-md-9">
		            <?= $this->form->submit('btn_save', lang('save'), 'class="btn btn-success"') ?>           
		            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"')  ?>
		      </div>
		</div>
		<?= $this->form->close() ?>
<?php endsection() ?>

<?php getview('personal_data') ?>