<?php section('content') ?>
    <h3 class="form-title">
        <?= lang('identity_cards') ?>
        <?= anchor(getGlobalVar('company')->slug . '/identity-cards/add', lang('add'), 'class="btn btn-default pull-right"') ?>
    </h3>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<?php foreach ($identity_cards->result() as $identity_card) { ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= $identity_card->description ?></b>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/identity-cards/edit/' . $identity_card->employee_identity_card_id, '<span class="glyphicon glyphicon-edit"></span> ' . lang('edit'), 'class="btn btn-warning btn-xs"') ?>
            <?= anchor(getGlobalVar('company')->slug . '/identity-cards/remove/' . $identity_card->employee_identity_card_id, '<span class="glyphicon glyphicon-trash"></span> ' . lang('remove'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= $identity_card->identity_card_id ?><br />
            <?= lang('published') ?> : <?= antiNull(toDate($identity_card->published)) ?><br />
            <?= lang('expired') ?> : <?= antiNull(toDate($identity_card->expired)) ?>
        </p>
    </div>
<?php } ?>
    <h3 class="form-title"><?= lang('waiting_approval') ?></h3>
<?php foreach ($requests->result() as $request) { ?>
    <?php $identity_card = json_decode($request->request_data) ?>
    <div class="data-list-item data-list-item-hover">
        <b><?= $identity_card->description ?></b> <?= labelRequestType($request->request_type) ?>
        <div class="btn-group pull-right">
            <?= anchor(getGlobalVar('company')->slug . '/request-changes/cancel/' . $request->employee_request_change_id, '<span class="fa fa-times"></span> ' . lang('cancel'), 'class="btn btn-danger btn-xs"') ?>
        </div>
        <br />
        <p>
            <?= $identity_card->identity_card_id ?><br />
            <?= lang('published') ?> : <?= antiNull(toDate($identity_card->published)) ?><br />
            <?= lang('expired') ?> : <?= antiNull(toDate($identity_card->expired)) ?>
        </p>
    </div>
<?php } ?>
<?php endsection() ?>
<?php getview('personal_data') ?>