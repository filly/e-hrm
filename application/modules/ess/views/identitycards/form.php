<?php section('content') ?>
<?php getview('template/partials/message') ?>
<?php getview('template/partials/validation') ?>
<?= $this->form->open('', 'class="form-horizontal"') ?>
    <h3 class="form-title"><?= lang('identity_cards') ?></h3>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('description') ?></label>
        <div class="col-md-6">
            <?= $this->form->text('description', null, 'placeholder="'.lang('enter_description').'" class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('identity_card_id') ?></label>
        <div class="col-md-6">
            <?= $this->form->text('identity_card_id', null, 'placeholder="'.lang('enter_identity_card_id').'" class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('published') ?></label>
        <div class="col-md-4">
            <div class="input-group">
                <?= $this->form->date('published', null, 'placeholder="'.lang('enter_published').'" class="form-control datepicker"') ?>
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"><?= lang('expired') ?></label>
        <div class="col-md-4">
            <div class="input-group">
                <?= $this->form->date('expired', null, 'placeholder="'.lang('enter_expired').'" class="form-control datepicker"') ?>
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= $this->form->submit('btn_save', lang('save'), 'class="btn btn-success"') ?>
            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"')  ?>
        </div>
    </div>
<?= $this->form->close() ?>
<?php endsection() ?>

<?php getview('personal_data') ?>