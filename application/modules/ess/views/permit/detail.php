<?php getview('template/header') ?>
<div class="box box-primary box-sm">
	<div class="box-header with-border">
		<h1 class="box-title">
			<?= lang('request_permit') ?>		
		</h1>
	</div>	
	<div class="box-body">						
		<?php getview('template/partials/message') ?>
		<?php getview('template/partials/validation') ?>
		<h3 class="form-title">
			<?= lang('detail') ?>
			<span class="pull-right"><?= labelRequestStatus($permit->permit_status) ?></span>
		</h3>
		<table class="table-info">
			<tr>
				<td width="200"><?= lang('name') ?></td>
				<td class="text-right"><?= $permit->first_name ?> <?= $permit->last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('employee_code') ?></td>
				<td class="text-right"><?= $permit->employee_code ?></td>
			</tr>
			<tr>
				<td><?= lang('permit_type') ?></td>
				<td class="text-right"><?= toSelect(lang('_permit_type'), $permit->permit_type) ?></td>
			</tr>
			<tr>
				<td><?= lang('permit_date') ?></td>
				<td class="text-right"><?= humanDateTime($permit->permit_date) ?></td>
			</tr>
			<tr>
				<td><?= lang('reason') ?></td>
				<td class="text-right"><?= $permit->reason ?></td>
			</tr>			
			<?php if ($permit->confirmed_by) { ?>
			<tr>
				<td><?= lang('confirmed_by') ?></td>
				<td class="text-right"><?= $permit->confirmed_by_first_name ?> <?= $permit->confirmed_by_last_name ?></td>
			</tr>
			<tr>
				<td><?= lang('confirmed_at') ?></td>
				<td class="text-right"><?= humanDateTime($permit->confirmed_at) ?></td>
			</tr>
			<?php } ?>			
		</table>			
		<?= anchor(getGlobalVar('company')->slug . '/permit', lang('back'), 'class="btn btn-default"') ?>
		<?php if ($permit->permit_status == 'waiting') { ?>
			<?= anchor(getGlobalVar('company')->slug . '/permit/cancel/' . $permit->permit_id, lang('cancel'), 'class="btn btn-danger pull-right"') ?>
		<?php } ?>
	</div>
</div>
<?php getview('template/footer') ?>