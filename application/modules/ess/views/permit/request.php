<?php section('content') ?>
	<?php getview('template/partials/message') ?>
	<?php getview('template/partials/validation') ?>
	<?= $this->form->open('', 'class="form-horizontal"') ?>
	<h3 class="form-title">
		<?= lang('send_request') ?>
		<?= anchor(getGlobalVar('company')->slug . '/permit', lang('my_request'), 'class="btn btn-default pull-right"') ?>
	</h3>
	<div class="form-group">
	    <label class="col-md-3 control-label"><?= lang('permit_type') ?></label>
	    <div class="col-md-4">
	        <?= $this->form->select('permit_type', lang('_permit_type'), null,'class="form-control"') ?>                        
	    </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"><?= lang('permit_date') ?></label>
	  <div class="col-md-5">
	  	<div class="input-group">
	  		<div class="input-group-addon">
	  			<span class="fa fa-calendar"></span>
	  		</div>
	    <?= $this->form->datetime('permit_date', null, 'placeholder="'.lang('enter_permit_date').'" class="form-control datetimepicker"') ?>
	    </div>
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"><?= lang('reason') ?></label>
	  <div class="col-md-5">
	    <?= $this->form->textarea('reason', null, 'placeholder="'.lang('enter_reason').'" class="form-control"') ?>
	  </div>
	</div>
	<div class="form-group">                        
	      <div class="col-md-offset-3 col-md-9">
	            <?= $this->form->submit('btn_send', lang('send'), 'class="btn btn-success"') ?>            
	            <?= $this->form->reset('btn_reset', lang('reset'), 'class="btn btn-default"') ?>  
	      </div>
	</div>	    			        			    	    			        			   		    			        		     			        			 
	<?= $this->form->close() ?>
<?php endsection() ?>	
<?php getview('permit') ?>	