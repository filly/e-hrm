<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leave extends EssController {

	protected $leave;

	public function __construct() {
		parent::__construct();		
		$this->load->library('leave/indonesia');
		$this->leave = $this->indonesia;
	}

	public function index() {
		$this->leave->index();
	}

	public function detail($leave_id) {
		$this->leave->detail($leave_id);
	}

	public function request() {
		$this->leave->request();
	}

	public function cancel($leave_id) {
		$this->leave->cancel($leave_id);
	}	

	public function confirmation($leave_id) {
		$this->leave->confirmation($leave_id);
	}

	public function approve($leave_id) {
		$this->leave->approve($leave_id);
	}

	public function reject($leave_id) {
		$this->leave->reject($leave_id);
	}

	public function suspend($leave_id) {
		$this->leave->suspend($leave_id);
	}

}