<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addresses extends EssController {

	public function __construct() {
		parent::__construct();		
		$this->load->model('employees_model');
	}

	public function index() {
		$addresses = $this->employees_model->getEmployeeAddresses(getAuth('employee_id'));
		$requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'address');
		$this->load->view('addresses/index', array(
			'addresses' => $addresses,
			'requests' => $requests
		));
	}

	public function add() {
		if ($post = $this->input->post()) {
			$this->validation();
			if ($this->employees_model->employeeRequestChanges('address', 'add', $post)) {
				$this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/addresses');
			} else {
				$this->redirect->with('errorMessage', lang('error_request_changes'))->back();
			}
		}
		$this->load->view('addresses/form');
	}

	public function edit($employee_address_id) {
		if (!$address = $this->employees_model->findEmployeeAddress($employee_address_id, getAuth('employee_id'))) {
			show_404();
		}
		if ($post = $this->input->post()) {
			$this->validation();
			$post['id'] = $employee_address_id;
			if ($this->employees_model->employeeRequestChanges('address', 'update', $post)) {
				$this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/addresses');
			} else {
				$this->redirect->with('errorMessage', lang('error_request_changes'))->back();
			}
		}		
		$this->form->setData($address);
		$this->load->view('addresses/form');
	}

	public function remove($employee_address_id) {
		if (!$address = $this->employees_model->findEmployeeAddress($employee_address_id, getAuth('employee_id'))) {
			show_404();
		}
		if ($this->employees_model->employeeRequestChanges('address', 'remove', $address)) {
			$this->redirect->with('successMessage', lang('success_request_changes'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_request_changes'))->back();
		}
	}

	private function validation() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('description', lang('description'), 'required');
		$this->form_validation->set_rules('address', lang('address'), 'required');
		$this->form_validation->set_rules('city', lang('city'), 'required');		
		$this->form_validation->set_rules('home_status', lang('home_status'), 'required');		
		if (!$this->form_validation->run()) {
			$this->redirect->withInput()->withValidation()->back();
		}
	}

}