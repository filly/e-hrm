<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends EssController
{

    /**
     * Documents constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employees_model');
    }

    public function index()
    {
        $documents = $this->employees_model->getEmployeeDocuments(getAuth('employee_id'));
        $requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'document');
        $this->load->view('documents/index', array(
            'documents' => $documents,
            'requests' => $requests
        ));
    }

    public function add() {
        if ($post = $this->input->post()) {
            $this->validation();
            if ($this->employees_model->employeeRequestChanges('document', 'add', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/documents');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->load->view('documents/form');
    }

    public function remove($employee_document_id) {
        if (!$document = $this->employees_model->findEmployeeDocument($employee_document_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($this->employees_model->employeeRequestChanges('document', 'remove', $document)) {
            $this->redirect->with('successMessage', lang('success_request_changes'))->back();
        } else {
            $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('document', lang('document'), 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }
    }
}