<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request_changes extends EssController {

	public function __construct() {
		parent::__construct();		
		$this->load->model('employees_model');
	}

	public function index() {
		$this->load->view('request_changes/index');
	}

	public function detail($employee_request_changes_id) {
		if (!$request = $this->employees_model->findRequestChanges($employee_request_changes_id, getAuth('employee_id'))) {
			show_404();
		}		
	}

	public function cancel($employee_request_changes_id) {
		if (!$request = $this->employees_model->findEmployeeRequestChange($employee_request_changes_id, getAuth('employee_id'))) {
			show_404();
		}	
		if ($this->employees_model->cancelRequestChanges($employee_request_changes_id)) {
			$this->redirect->with('successMessage', lang('success_cancel_request_changes'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_cancel_request_changes'))->back();
		}
	}

}