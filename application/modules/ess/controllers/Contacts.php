<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends EssController {

	public function __construct() {
		parent::__construct();		
		$this->load->model('employees_model');
	}

	public function index() {		
		if ($post = $this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('media', lang('media'), 'required');
			$this->form_validation->set_rules('contact_type', lang('contact_type'), 'required');
			if ($this->input->post('media') == 'email') {
				$this->form_validation->set_rules('description', lang('contact'), 'required|valid_email');
			} else {
				$this->form_validation->set_rules('description', lang('contact'), 'required');
			}			
			if (!$this->form_validation->run()) {
				$this->redirect->withInput()->withValidation()->back();
			}
			if ($this->employees_model->employeeRequestChanges('contact', 'add', $post)) {
				$this->redirect->with('successMessage', lang('success_request_changes'))->back();
			} else {
				$this->redirect->with('errorMessage', lang('error_request_changes'));
			}
		}
		$contacts = $this->employees_model->getEmployeeContacts(getAuth('employee_id'));
		$requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'contact');
		$this->load->view('contacts/index', array(
			'contacts' => $contacts,
			'requests' => $requests
		));
	}

	public function remove($employee_contact_id) {
		if (!$contact = $this->employees_model->findEmployeeContact($employee_contact_id, getAuth('employee_id'))) {
			show_404();
		}
		if ($this->employees_model->employeeRequestChanges('contact', 'remove', $contact)) {
			$this->redirect->with('successMessage', lang('success_request_changes'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_request_changes'))->back();
		}
	}


}