<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Families extends EssController {

	public function __construct() {
		parent::__construct();	
		$this->load->model('employees_model');
	}

	public function index() {
		$families = $this->employees_model->getEmployeeFamilies(getAuth('employee_id'));
		$requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'family');
		$this->load->view('families/index', array(
			'families' => $families,
			'requests' => $requests
		));
	}

	public function add() {
		if ($post = $this->input->post()) {
			$this->validation();
			if ($this->employees_model->employeeRequestChanges('family', 'add', $post)) {
				$this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/families');
			} else {
				$this->redirect->with('errorMessage', lang('error_request_changes'))->back();
			}
		}
		$this->load->view('families/form');
	}

	private function validation() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('relationship', lang('relationship'), 'required');
		$this->form_validation->set_rules('name', lang('name'), 'required');
		if (!$this->form_validation->run()) {
			$this->redirect->withInput()->withValidation()->back();
		}
	}

}