<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training extends EssController {

	public function __construct() {
		parent::__construct();	
		$this->load->model('training_model');
	}

	public function index() {
		if ($this->input->is_ajax_request()) {
			$this->load->library('datatables');			

			$response = $this->datatables->collection(function(){
				if ($filter = $this->input->get('filter')) {				
					if ($filter['request_status'] <> '') {
						$this->db->where('training_status', $filter['request_status']);
					}
				}
				$this->db->where('created_by', getAuth('employee_id'))							
				->from('training');
			})
			->showColumns('created_at, training, training_start_date, training_end_date, cost, place, trainer, training_status')
			->editColumn('created_at', function($data) {
				return toDate($data->created_at);
			})
			->editColumn('training_start_date', function($data) {
				return humanDateTime($data->training_start_date);
			})			
			->editColumn('training_end_date', function($data) {
				return humanDateTime($data->training_end_date);
			})			
			->editColumn('cost', function($data){
				return toCurrency($data->cost);
			})
			->editColumn('training_status', function($data) {
				return labelRequestStatus($data->training_status);						
			})
			->addColumn('', function($data){
				return '<a href="'.base_url(getGlobalVar('company')->slug . '/training/detail/' . $data->training_id).'"><span class="fa fa-search"></span></a>';							
			})
			->searchableColumns('training, place, trainer')												
			->render();			
			return $this->output->set_content_type('application/json')->set_output($response);
		}
		$this->load->view('training/index');
	}

	public function detail($training_id) {
		if (!$training = $this->training_model->detail($training_id)) {
			show_404();
		}
		$employees = $this->training_model->getEmployees($training_id);
		$this->load->view('training/detail', array(
			'training' => $training,
			'employees' => $employees
		));
	}

	public function confirmation($training_id) {
		if (!$training = $this->training_model->confirmation($training_id)) {
			show_404();
		}
		$employees = $this->training_model->getEmployees($training_id);
		$this->load->view('training/confirmation', array(
			'training' => $training,
			'employees' => $employees
		));
	}

	public function request() {
		if ($post = $this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('training', lang('training'), 'required');
			$this->form_validation->set_rules('training_start_date', lang('training_date'), 'required');
			$this->form_validation->set_rules('training_end_date', lang('training_date'), 'required');
			$this->form_validation->set_rules('cost', lang('cost'), 'required');
			$this->form_validation->set_rules('place', lang('place'), 'required');
			$this->form_validation->set_rules('trainer', lang('trainer'), 'required');
			if (!$this->form_validation->run()) {
				$this->redirect->withInput()->withValidation()->back();
			}
			if ($this->training_model->request($post['training'], $post['training_start_date'], $post['training_end_date'], $post['cost'], $post['place'], $post['trainer'])) {
				$this->redirect->with('successMessage', lang('success_send_request_training'))->to(getGlobalVar('company')->slug . '/training');
			} else {
				$this->redirect->with('errorMessage', lang('error_send_request_training'))->back();
			}
		}
		$this->load->view('training/request');
	}

	public function cancel($training_id) {
		if (!$request = $this->training_model->findTraining($training_id, getAuth('employee_id'))) {
			show_404();
		}	
		if ($this->training_model->cancel($training_id)) {
			$this->redirect->with('successMessage', lang('success_cancel_request_training'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_cancel_request_training'))->back();
		}
	}	

	public function approve($overtime_id) {
		if (!$permit = $this->training_model->confirmation($overtime_id)) {
			show_404();
		}
		if ($this->training_model->approve($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_approve_request_training'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_approve_request_training'))->back();
		}
	}

	public function reject($overtime_id) {
		if (!$permit = $this->training_model->confirmation($overtime_id)) {
			show_404();
		}
		if ($this->training_model->reject($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_reject_request_training'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_reject_request_training'))->back();
		}
	}

	public function suspend($overtime_id) {
		if (!$permit = $this->training_model->confirmation($overtime_id)) {
			show_404();
		}
		if ($this->training_model->suspend($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_suspend_request_training'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_suspend_request_training'))->back();
		}
	}

}