<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Experiences extends EssController
{

    /**
     * Experiences constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employees_model');
    }

    public function index()
    {
        $experiences = $this->employees_model->getEmployeeExperiences(getAuth('employee_id'));
        $requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'experience');
        $this->load->view('Experiences/index', array(
            'experiences' => $experiences,
            'requests' => $requests
        ));
    }

    public function add() {
        if ($post = $this->input->post()) {
            $this->validation();
            if ($this->employees_model->employeeRequestChanges('experience', 'add', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/experiences');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->load->view('Experiences/form');
    }

    public function edit($employee_experience_id) {
        if (!$experience = $this->employees_model->findEmployeeExperience($employee_experience_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($post = $this->input->post()) {
            $this->validation();
            $post['id'] = $employee_experience_id;
            if ($this->employees_model->employeeRequestChanges('experience', 'update', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/experiences');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->form->setData($experience);
        $this->load->view('Experiences/form');
    }

    public function remove($employee_experience_id) {
        if (!$experience = $this->employees_model->findEmployeeExperience($employee_experience_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($this->employees_model->employeeRequestChanges('experience', 'remove', $experience)) {
            $this->redirect->with('successMessage', lang('success_request_changes'))->back();
        } else {
            $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('workplace', lang('workplace'), 'required');
        $this->form_validation->set_rules('occupation', lang('occupation'), 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }
    }
}