<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends EssController {

	public function __construct() {
		parent::__construct();		
	}
	
	public function viewed_approval() {		
	}

	public function approval() {
		$query = '
			SELECT leave_id as request_id, `leave`.employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, leave_status as request_status, "request" AS request_type, "leave" AS data
				FROM `leave`
				JOIN employees ON employees.employee_id = `leave`.employee_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
				JOIN level ON level.level_id = employees.level_id			
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE `leave`.company_id = '.getGlobalVar('company')->company_id.'	
				AND `leave`.employee_id = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL				
			UNION ALL
			SELECT permit_id as request_id, permit.employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, permit_status as request_status, "request" AS request_type, "permit" AS data 
				FROM permit					
				JOIN employees ON employees.employee_id = permit.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE permit.company_id = '.getGlobalVar('company')->company_id.'
				AND permit.employee_id = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL
			UNION ALL
			SELECT overtime_id as request_id, created_by as employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, overtime_status as request_status, "request" AS request_type, "overtime" AS data
				FROM overtime
				JOIN employees ON employees.employee_id = overtime.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id		
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE overtime.company_id = '.getGlobalVar('company')->company_id.'
				AND overtime.created_by = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL										
			UNION ALL
			SELECT training_id as request_id, created_by as employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, training_status as request_status, "request" AS request_type, "training" AS data 
				FROM training
				JOIN employees ON employees.employee_id = training.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id	
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE training.company_id = '.getGlobalVar('company')->company_id.'
				AND training.created_by = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL							
			UNION ALL	
			SELECT employee_request_change_id as request_id, employee_request_changes.employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, viewed, request_status, request_type, data
				FROM employee_request_changes
				JOIN employees ON employees.employee_id = employee_request_changes.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id	
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE employee_request_changes.company_id = '.getGlobalVar('company')->company_id.'	
				AND employee_request_changes.employee_id ="'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL					
			ORDER BY confirmed_at DESC																		
		';
		
		$response = array(
			'total' => $this->db->from('('.$query.') request')->where('viewed', 'false')->count_all_results(),			
		);				
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	public function approval_list() {
		$query = '
			SELECT leave_id as request_id, `leave`.employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, leave_status as request_status, "request" AS request_type, "leave" AS data
				FROM `leave`
				JOIN employees ON employees.employee_id = `leave`.employee_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
				JOIN level ON level.level_id = employees.level_id			
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE `leave`.company_id = '.getGlobalVar('company')->company_id.'	
				AND `leave`.employee_id = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL				
			UNION ALL
			SELECT permit_id as request_id, permit.employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, permit_status as request_status, "request" AS request_type, "permit" AS data 
				FROM permit					
				JOIN employees ON employees.employee_id = permit.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE permit.company_id = '.getGlobalVar('company')->company_id.'
				AND permit.employee_id = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL
			UNION ALL
			SELECT overtime_id as request_id, created_by as employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, overtime_status as request_status, "request" AS request_type, "overtime" AS data
				FROM overtime
				JOIN employees ON employees.employee_id = overtime.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id		
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE overtime.company_id = '.getGlobalVar('company')->company_id.'
				AND overtime.created_by = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL										
			UNION ALL
			SELECT training_id as request_id, created_by as employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, viewed, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, training_status as request_status, "request" AS request_type, "training" AS data 
				FROM training
				JOIN employees ON employees.employee_id = training.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id	
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE training.company_id = '.getGlobalVar('company')->company_id.'
				AND training.created_by = "'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL							
			UNION ALL	
			SELECT employee_request_change_id as request_id, employee_request_changes.employee_id, employee_identities.first_name, employee_identities.last_name, employee_code, created_at, updated_at, confirmed_by, confirmed_by.first_name as confirmed_by_name, confirmed_at, viewed, request_status, request_type, data
				FROM employee_request_changes
				JOIN employees ON employees.employee_id = employee_request_changes.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id	
				LEFT JOIN employee_identities confirmed_by ON confirmed_by.employee_id = confirmed_by
				WHERE employee_request_changes.company_id = '.getGlobalVar('company')->company_id.'	
				AND employee_request_changes.employee_id ="'.getAuth('employee_id').'"
				AND confirmed_by IS NOT NULL					
			ORDER BY confirmed_at DESC																		
		';
		
		$response = array(	
			'total' => 0,
			'data' => array()
		);		
		$requests = $this->db->query($query . ' LIMIT 10');
		foreach ($requests->result() as $request) {
			$since = Carbon\Carbon::createFromDate(
				date('Y', strtotime($request->confirmed_at)),
				date('m', strtotime($request->confirmed_at)),
				date('d', strtotime($request->confirmed_at))
			)->diffForHumans();	
			$detail_url = urlRequestDetail($request->data, $request->request_id);						
			$response['data'][] = array(				
				'request_type' => toSelect(lang('_request_type'), $request->request_type),
				'data' => toSelect(lang('_request_data'), $request->data),
				'request_status' => labelRequestStatus($request->request_status),
				'confirmed_by' => $request->confirmed_by_name,
				'since' => $since,
				'detail_url' => base_url($detail_url)
			);
		}
		$this->db->where('viewed', 'false')->update('employee_request_changes', array('viewed' => 'true'));
		$this->db->where('viewed', 'false')->update('leave', array('viewed' => 'true'));
		$this->db->where('viewed', 'false')->update('overtime', array('viewed' => 'true'));
		$this->db->where('viewed', 'false')->update('permit', array('viewed' => 'true'));
		$this->db->where('viewed', 'false')->update('training', array('viewed' => 'true'));				
		$response['total'] = $this->db->from('('.$query.') request')->where('viewed', 'false')->count_all_results();
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

}