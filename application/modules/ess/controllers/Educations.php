<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Educations extends EssController
{

    /**
     * Educations constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employees_model');
    }

    public function index()
    {
        $educations = $this->employees_model->getEmployeeEducations(getAuth('employee_id'));
        $requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'education');
        $this->load->view('educations/index', array(
            'educations' => $educations,
            'requests' => $requests
        ));
    }

    public function add() {
        if ($post = $this->input->post()) {
            $this->validation();
            if ($this->employees_model->employeeRequestChanges('education', 'add', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/educations');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->load->view('educations/form');
    }

    public function edit($employee_education_id) {
        if (!$education = $this->employees_model->findEmployeeEducation($employee_education_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($post = $this->input->post()) {
            $this->validation();
            $post['id'] = $employee_education_id;
            if ($this->employees_model->employeeRequestChanges('education', 'update', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/educations');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->form->setData($education);
        $this->load->view('educations/form');
    }

    public function remove($employee_education_id) {
        if (!$education = $this->employees_model->findEmployeeEducation($employee_education_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($this->employees_model->employeeRequestChanges('education', 'remove', $education)) {
            $this->redirect->with('successMessage', lang('success_request_changes'))->back();
        } else {
            $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('grade', lang('grade'), 'required');
        $this->form_validation->set_rules('major', lang('major'), 'required');
        $this->form_validation->set_rules('institution', lang('institution'), 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }
    }
}