<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Identity_cards extends EssController
{

    /**
     * identity_cards constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employees_model');
    }

    public function index()
    {
        $identity_cards = $this->employees_model->getEmployeeIdentityCards(getAuth('employee_id'));
        $requests = $this->employees_model->getEmployeeWaitingRequestChanges(getAuth('employee_id'), 'identity_card');
        $this->load->view('identitycards/index', array(
            'identity_cards' => $identity_cards,
            'requests' => $requests
        ));
    }

    public function add() {
        if ($post = $this->input->post()) {
            $this->validation();
            if ($this->employees_model->employeeRequestChanges('identity_card', 'add', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/identity-cards');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->load->view('identitycards/form');
    }

    public function edit($employee_identity_card_id) {
        if (!$identity_card = $this->employees_model->findEmployeeIdentityCard($employee_identity_card_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($post = $this->input->post()) {
            $this->validation();
            $post['id'] = $employee_identity_card_id;
            if ($this->employees_model->employeeRequestChanges('identity_card', 'update', $post)) {
                $this->redirect->with('successMessage', lang('success_request_changes'))->to(getGlobalVar('company')->slug . '/identity-cards');
            } else {
                $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
            }
        }
        $this->form->setData($identity_card);
        $this->load->view('identity_cards/form');
    }

    public function remove($employee_identity_card_id) {
        if (!$identity_card = $this->employees_model->findEmployeeIdentityCard($employee_identity_card_id, getAuth('employee_id'))) {
            show_404();
        }
        if ($this->employees_model->employeeRequestChanges('identity_card', 'remove', $identity_card)) {
            $this->redirect->with('successMessage', lang('success_request_changes'))->back();
        } else {
            $this->redirect->with('errorMessage', lang('error_request_changes'))->back();
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('description', lang('description'), 'required');
        $this->form_validation->set_rules('identity_card_id', lang('identity_card_id'), 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }
    }
}