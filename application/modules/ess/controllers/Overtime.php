<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overtime extends EssController {

	public function __construct() {
		parent::__construct();	
		$this->load->model('overtime_model');
	}

	public function index() {
		if ($this->input->is_ajax_request()) {
			$this->load->library('datatables');			

			$response = $this->datatables->collection(function(){
				if ($filter = $this->input->get('filter')) {				
					if ($filter['request_status'] <> '') {
						$this->db->where('overtime_status', $filter['request_status']);
					}
				}
				$this->db->where('created_by', getAuth('employee_id'))							
				->from('overtime');
			})
			->showColumns('created_at, overtime_date, start_overtime, end_overtime, reason, description, request_status')
			->editColumn('created_at', function($data) {
				return toDate($data->created_at);
			})
			->editColumn('overtime_date', function($data) {
				return toDate($data->overtime_date);
			})			
			->editColumn('request_status', function($data) {
				return labelRequestStatus($data->overtime_status);					
			})
			->addColumn('', function($data){
				return '<a href="'.base_url(getGlobalVar('company')->slug . '/overtime/detail/' . $data->overtime_id).'"><span class="fa fa-search"></span></a>';							
			})
			->searchableColumns('reason, description')												
			->render();			
			return $this->output->set_content_type('application/json')->set_output($response);
		}
		$this->load->view('overtime/index');
	}

	public function detail($overtime_id) {
		if (!$overtime = $this->overtime_model->detail($overtime_id)) {
			show_404();
		}
		$employees = $this->overtime_model->getEmployees($overtime_id);
		$this->load->view('overtime/detail', array(
			'overtime' => $overtime,
			'employees' => $employees
		));
	}

	public function confirmation($overtime_id) {
		if (!$overtime = $this->overtime_model->confirmation($overtime_id)) {
			show_404();
		}
		$employees = $this->overtime_model->getEmployees($overtime_id);
		$this->load->view('overtime/confirmation', array(
			'overtime' => $overtime,
			'employees' => $employees
		));
	}

	public function request() {
		if ($post = $this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('overtime_date', lang('overtime_date'), 'required');
			$this->form_validation->set_rules('start_overtime', lang('time_overtime'), 'required');
			$this->form_validation->set_rules('end_overtime', lang('time_overtime'), 'required');
			$this->form_validation->set_rules('reason', lang('reason'), 'required');
			if (!$this->form_validation->run()) {
				$this->redirect->withInput()->withValidation()->back();
			}
			if ($this->overtime_model->request($post['overtime_date'], $post['start_overtime'], $post['end_overtime'], $post['reason'], $post['description'])) {
				$this->redirect->with('successMessage', lang('success_send_request_overtime'))->to(getGlobalVar('company')->slug . '/overtime');
			} else {
				$this->redirect->with('errorMessage', lang('error_send_request_overtime'))->back();
			}
		}
		$this->load->view('overtime/request');
	}	

	public function approve($overtime_id) {
		if (!$permit = $this->overtime_model->confirmation($overtime_id)) {
			show_404();
		}
		if ($this->overtime_model->approve($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_approve_request_overtime'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_approve_request_overtime'))->back();
		}
	}

	public function reject($overtime_id) {
		if (!$permit = $this->overtime_model->confirmation($overtime_id)) {
			show_404();
		}
		if ($this->overtime_model->reject($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_reject_request_overtime'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_reject_request_overtime'))->back();
		}
	}

	public function suspend($overtime_id) {
		if (!$permit = $this->overtime_model->confirmation($overtime_id)) {
			show_404();
		}
		if ($this->overtime_model->suspend($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_suspend_request_overtime'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_suspend_request_overtime'))->back();
		}
	}


	public function cancel($overtime_id) {
		if (!$request = $this->overtime_model->findOvertime($overtime_id, getAuth('employee_id'))) {
			show_404();
		}	
		if ($this->overtime_model->cancel($overtime_id)) {
			$this->redirect->with('successMessage', lang('success_cancel_request_overtime'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_cancel_request_overtime'))->back();
		}
	}

}