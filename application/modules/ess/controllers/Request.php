<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends EssController {

	public function __construct() {
		parent::__construct();		
	}


	public function index() {
		if ($this->input->is_ajax_request()) {
			$this->load->library('datatables');
			
			$response = $this->datatables->collection(function() {
				if ($filter = $this->input->get('filter')) {				
					if ($filter['data'] <> '') {
						$this->db->where('data', $filter['data']);
					}
					if ($filter['request_type'] <> '') {
						$this->db->where('request_typea', $filter['request_type']);
					}
					if ($filter['request_status'] <> '') {
						$this->db->where('request_status', $filter['request_status']);
					}
				}
				$this->db->from('(										
					SELECT leave_id as request_id, `leave`.employee_id, first_name, last_name, employee_code, created_at, leave_status as request_status, "request" AS request_type, "leave" AS data
						FROM `leave`
						JOIN employees ON employees.employee_id = `leave`.employee_id
						JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
						JOIN level ON level.level_id = employees.level_id								
						WHERE grade < '.getAuth('grade').'						
						AND `leave`.company_id = '.getGlobalVar('company')->company_id.'		
					UNION ALL
					SELECT permit_id as request_id, permit.employee_id, first_name, last_name, employee_code, created_at, permit_status as request_status, "request" AS request_type, "permit" AS data 
						FROM permit					
						JOIN employees ON employees.employee_id = permit.employee_id
						JOIN level ON level.level_id = employees.level_id
						JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
						WHERE grade < '.getAuth('grade').'	
						AND permit.company_id = '.getGlobalVar('company')->company_id.'									
					UNION ALL
					SELECT overtime_id as request_id, created_by as employee_id, first_name, last_name, employee_code, created_at, overtime_status as request_status, "request" AS request_type, "overtime" AS data
						FROM overtime
						JOIN employees ON employees.employee_id = overtime.created_by
						JOIN level ON level.level_id = employees.level_id
						JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
						WHERE grade < '.getAuth('grade').'	
						AND overtime.company_id = '.getGlobalVar('company')->company_id.'									
					UNION ALL
					SELECT training_id as request_id, created_by as employee_id, first_name, last_name, employee_code, created_at, training_status as request_status, "request" AS request_type, "training" AS data 
						FROM training
						JOIN employees ON employees.employee_id = training.created_by
						JOIN level ON level.level_id = employees.level_id
						JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
						WHERE grade < '.getAuth('grade').'
						AND training.company_id = '.getGlobalVar('company')->company_id.'														
					UNION ALL	
					SELECT employee_request_change_id as request_id, employee_request_changes.employee_id, first_name, last_name, employee_code, created_at, request_status, request_type, data
						FROM employee_request_changes
						JOIN employees ON employees.employee_id = employee_request_changes.employee_id
						JOIN level ON level.level_id = employees.level_id
						JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
						WHERE grade < '.getAuth('grade').'	
						AND employee_request_changes.company_id = '.getGlobalVar('company')->company_id.'													
				) request');																
			})
			->showColumns('employee_code, first_name, created_at, request_type, request_status')
			->editColumn('first_name', function($data) {
				return $data->first_name . ' ' . $data->last_name;
			})
			->editColumn('created_at', function($data) {
				return humanDate($data->created_at);
			})
			->editColumn('request_type', function($data) {
				return toSelect(lang('_request_type'), $data->request_type) . ' ' .toSelect(lang('_request_data'), $data->data);
			})		
			->editColumn('request_status', function($data) {
				return labelRequestStatus($data->request_status);				
			})
			->addColumn('', function($data) {
				$detail_url = urlRequestConfirmation($data->data, $data->request_id);						
				
				return anchor($detail_url, '<span class="fa fa-search"></span> ', 'class="text-primary"');
			})
			->searchableColumns('employee_code, first_name, last_name')
			->render();			
			return $this->output->set_content_type('application/json')->set_output($response);;
		}
		$this->load->view('request');
	}

}