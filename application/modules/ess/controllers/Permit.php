<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permit extends EssController {

	public function __construct() {
		parent::__construct();	
		$this->load->model('permit_model');
	}

	public function index() {
		if ($this->input->is_ajax_request()) {
			$this->load->library('datatables');			

			$response = $this->datatables->collection(function(){
				if ($filter = $this->input->get('filter')) {				
					if ($filter['request_status'] <> '') {
						$this->db->where('permit_status', $filter['request_status']);
					}
				}
				$this->db->where('employee_id', getAuth('employee_id'))							
				->from('permit');
			})
			->showColumns('created_at, permit_type, permit_date, reason, permit_status')
			->editColumn('created_at', function($data) {
				return toDate($data->created_at);
			})
			->editColumn('permit_type', function($data) {
				return toSelect(lang('_permit_type'), $data->permit_type);
			})
			->editColumn('permit_date', function($data) {
				return humanDateTime($data->permit_date);
			})					
			->editColumn('permit_status', function($data) {
				return labelRequestStatus($data->permit_status);								
			})
			->addColumn('', function($data){
				return '<a href="'.base_url(getGlobalVar('company')->slug . '/permit/detail/' . $data->permit_id).'"><span class="fa fa-search"></span></a>';							
			})
			->searchableColumns('reason')												
			->render();			
			return $this->output->set_content_type('application/json')->set_output($response);
		}
		$this->load->view('permit/index');
	}

	public function detail($permit_id) {
		if (!$permit = $this->permit_model->detail($permit_id)) {
			show_404();
		}
		$this->load->view('permit/detail', array(
			'permit' => $permit
		));
	}

	public function confirmation($permit_id) {
		if (!$permit = $this->permit_model->confirmation($permit_id)) {
			show_404();
		}
		$this->load->view('permit/confirmation', array(
			'permit' => $permit
		));
	}

	public function request() {
		if ($post = $this->input->post()) {
			$this->load->library('form_validation');			
			$this->form_validation->set_rules('permit_type', lang('permit_type'), 'required');
			$this->form_validation->set_rules('permit_date', lang('permit_date'), 'required');						
			$this->form_validation->set_rules('reason', lang('reason'), 'required');			
			if (!$this->form_validation->run()) {
				$this->redirect->withInput()->withValidation()->back();
			}
			if ($this->permit_model->request($post['permit_type'], $post['permit_date'], $post['reason'])) {
				$this->redirect->with('successMessage', lang('success_send_request_permit'))->to(getGlobalVar('company')->slug . '/permit');
			} else {
				$this->redirect->with('errorMessage', lang('error_send_request_permit'))->back();
			}
		}
		$this->load->view('permit/request');
	}

	public function approve($permit_id) {
		if (!$permit = $this->permit_model->confirmation($permit_id)) {
			show_404();
		}
		if ($this->permit_model->approve($permit_id)) {
			$this->redirect->with('successMessage', lang('success_approve_request_permit'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_approve_request_permit'))->back();
		}
	}

	public function reject($permit_id) {
		if (!$permit = $this->permit_model->confirmation($permit_id)) {
			show_404();
		}
		if ($this->permit_model->reject($permit_id)) {
			$this->redirect->with('successMessage', lang('success_reject_request_permit'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_reject_request_permit'))->back();
		}
	}

	public function suspend($permit_id) {
		if (!$permit = $this->permit_model->confirmation($permit_id)) {
			show_404();
		}
		if ($this->permit_model->suspend($permit_id)) {
			$this->redirect->with('successMessage', lang('success_suspend_request_permit'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_suspend_request_permit'))->back();
		}
	}

	public function cancel($permit_id) {
		if (!$request = $this->permit_model->findPermit($permit_id, getAuth('employee_id'))) {
			show_404();
		}	
		if ($this->permit_model->cancel($permit_id)) {
			$this->redirect->with('successMessage', lang('success_cancel_request_permit'))->back();
		} else {
			$this->redirect->with('errorMessage', lang('error_cancel_request_permit'))->back();
		}
	}

}