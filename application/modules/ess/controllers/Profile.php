<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends EssController {
	public function __construct() {
		parent::__construct();		
		$this->load->model('employees_model');
	}

	public function index() {
		if ($post = $this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('first_name', lang('first_name'), 'required');
			$this->form_validation->set_rules('last_name', lang('last_name'), 'required');
			$this->form_validation->set_rules('gender', lang('gender'), 'required');
			$this->form_validation->set_rules('blood_type', lang('blood_type'), 'required');
			$this->form_validation->set_rules('height', lang('height'), 'numeric');			
			$this->form_validation->set_rules('weight', lang('weight'), 'numeric');
			if (!$this->form_validation->run()) {
				$this->redirect->withInput()->withValidation()->back();
			}
			if ($this->employees_model->employeeRequestChanges('profile', 'update', $post)) {
				$this->redirect->with('successMessage', lang('success_request_changes'))->back();
			} else {
				$this->redirect->with('errorMessage', lang('error_request_changes'))->back();
			}

		}
		$identity = $this->employees_model->findEmployeeIdentity(getAuth('employee_id'));
		$this->form->setData($identity);
		$this->load->view('profile/index');
	}

	public function updatephoto() {
		$response = array();
		if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] <> '') {
			$this->load->library('upload', array(
				'upload_path' => './public/images/profile',
				'allowed_types' => 'gif|jpg|jpeg|png',
				'max_size' => '1024',
				'encrypt_name' => true
			));
			if ($this->upload->do_upload('photo')) {						
				$this->load->library('image_lib', array(
					'image_library' => 'gd2',
					'source_image' => $this->upload->data('full_path'),
					'width' => '400',
					'height' => '400',
					'maintain_ratio' => true,
					'x_axis' => ($this->upload->data('image_width') - 400) / 2,
					'x_axis' => ($this->upload->data('image_width') - 400) / 2
				));
				$this->image_lib->crop();				
				$request = array(
					'photo' => 'public/images/' . $this->upload->data('file_name')
				);
				if ($this->employees_model->requestChanges('photo_profile', 'update', $request)) {
					$response = array('success' => base_url('public/images/profile/' . $this->upload->data('file_name')));
				} else {
					$response = array('error' => lang('error_request_changes'));
				}							 
			} else{
				$response = array('error' => $this->upload->display_errors('', ''));
			}
		} else{
			$response = array('error' => lang('error_no_file_uploaded'));
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}


}