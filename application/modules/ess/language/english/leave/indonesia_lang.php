<?php

$lang = array(
	'request_leave' => 'Permintaan Cuti',

	'leave_type' => 'Jenis Cuti',

	'leave_periode' => 'Periode Cuti',
	'leave_start_date' => 'Mulai Cuti',
    'leave_end_date' => 'Selesai Cuti',

	'enter_start_date' => 'Tanggal Mulai',
	'enter_end_date' => 'Tanggal Selesai',

	'success_send_request_leave' => 'Berhasil mengirim permintaan cuti.',	
	'success_cancel_request_leave' => 'Berhasil membatalkan permintaan cuti.',
	'error_send_request_leave' => 'Gagal mengirim permintaan cuti.',
	'error_cancel_request_leave' => 'Gagal membatalkan permintaan cuti.',

	'_leave_type' => array(
		'' => 'Pilih Jenis Cuti',
		'annual_leave' => 'Cuti Tahunan',
		'non_annual' => 'Non Tahunan'
	)

);