<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indonesia_model extends CI_Model {

	public function __construct() {
		
	}

	public function findLeave($leave_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$leave = $this->db->where('leave_id', $leave_id)
						  ->get('leave')
						  ->row();
		return $leave;						  
	}

	public function detail($leave_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.employee_id')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   
						   ->where('a.employee_id', getAuth('employee_id'))
						   ->where('leave_id', $leave_id)
						   ->get('leave a')
						   ->row();
		return $detail;						   
	}

	public function confirmation($leave_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.employee_id')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   
						   ->where('grade < ', getAuth('grade'))
						   ->where('leave_id', $leave_id)
						   ->get('leave a')
						   ->row();
		return $detail;						   
	}

	public function request($type, $start, $end, $description) {
		$start = Carbon\Carbon::create(
			date('Y', strtotime($start)),
			date('m', strtotime($start)),
			date('d', strtotime($start))
		);

		$end = Carbon\Carbon::create(
			date('Y', strtotime($end)),
			date('m', strtotime($end)),
			date('d', strtotime($end))
		);
		$request = $this->db->insert('leave', array(
			'leave_type' => $type,
			'leave_start_date' => date('Y-m-d', strtotime($start)),
			'leave_end_date' => date('Y-m-d', strtotime($end)),
			'days' => $start->diffInDays($end) + 1,
			'description' => $description,
			'company_id' => getGlobalVar('company')->company_id,
			'employee_id' => getAuth('employee_id'),
			'leave_status' => 'waiting',
			'viewed' => 'true',
			'created_at' => date('Y-m-d H:i:s')
		));
		return $request;
	}	

	public function cancel($leave_id) {
		$cancel = $this->db->where('leave_id', $leave_id)
						  ->update('leave', array(
						  		'leave_status' => 'canceled'
						  	));
		return $cancel;					  
	}	

	public function approve($leave_id) {
		$approve = $this->db->where('leave_id', $leave_id)
				 ->update('leave', array(
				 	'leave_status' => 'approved',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')				 					 	
				 ));
		return $approve;				 
	}

	public function reject($leave_id) {
		$reject = $this->db->where('leave_id', $leave_id)
				 ->update('leave', array(
				 	'leave_status' => 'rejected',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),		
				 	'updated_at' => date('Y-m-d H:i:s')				 					 			 	
				 ));
		return $reject;					
	}

	public function suspend($leave_id) {
		$suspend = $this->db->where('leave_id', $leave_id)
				 ->update('leave', array(
				 	'leave_status' => 'waiting',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')				 					 					 	
				 ));
		return $suspend;					
	}
	
}