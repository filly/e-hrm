<?php getview('template/header') ?>	
	<div class="box box-primary">
		<div class="box-header with-border">
			<h1 class="box-title"><?= lang('request') ?></h1>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">				
					<ul class="box-nav">							
						<li><h3><?= lang('request') ?></h3></li>
						<li><a href="<?= base_url(getGlobalVar('company')->slug . '/request/waiting') ?>"><?= lang('waiting') ?></a></li>        				          
					</ul>					
				</div>
				<div class="col-md-9 form-wrapper">
					<?= render('content') ?>	
				</div>
			</div>			
		</div>
	</div>	
<?php getview('template/footer') ?>