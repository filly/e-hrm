<?php section('css') ?>
	<link href="<?= base_url('public/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
<?php endsection() ?>

<?php section('script') ?>
<script src="<?= base_url('public/plugins/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?= base_url('public/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
<script src="<?= base_url('public/dist/js/mss.employees.js') ?>"></script>
<?php endsection() ?>

<?php getview('template/header') ?>	
	<div class="box box-primary">
		<div class="box-header with-border">
			<h1 class="box-title"><?= lang('request') ?></h1>
		</div>
		<div class="box-body">			
			<div class="form-filter">
				<div class="row">
					<div class="col-md-4">	
						<?= $this->form->select('filter[division_id]', sourceDivisions(), null,'class="form-control"') ?>                        		
					</div>
					<div class="col-md-4">	
						<?= $this->form->select('filter[department_id]', sourceDepartments(), null,'class="form-control"') ?>                        		
					</div>
					<div class="col-md-4">													
						<label><?= lang('active') ?></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="filter[active][]" value="true" checked> <label><?= lang('_boolean')['true'] ?></label>&nbsp;
						<input type="checkbox" name="filter[active][]" value ="false" checked> <label><?= lang('_boolean')['false'] ?></label>&nbsp;
					</div>
				</div>		
			</div>
			<table id="dataTables" class="table table-hover">
				<thead>
					<tr>
						<td width="120"><?= lang('employee_code') ?></td>
						<td width="150"><?= lang('name') ?></td>			
						<td><?= lang('division') ?></td>			
						<td><?= lang('department') ?></td>			
						<td><?= lang('class') ?></td>			
						<td><?= lang('position') ?></td>			
						<td><?= lang('level') ?></td>			
						<td><?= lang('employee_status') ?></td>	
						<td><?= lang('active') ?></td>	
						<td></td>								
					</tr>
				</thead>
			</table>		
		</div>
	</div>	
<?php getview('template/footer') ?>