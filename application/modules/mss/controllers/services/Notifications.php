<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends MssController {

	public function __construct() {
		parent::__construct();		
	}
	
	public function request() {
		$query = '
			SELECT leave_id as request_id, `leave`.employee_id, first_name, last_name, employee_code, created_at, leave_status as request_status, "request" AS request_type, "leave" AS data
				FROM `leave`
				JOIN employees ON employees.employee_id = `leave`.employee_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
				JOIN level ON level.level_id = employees.level_id								
				WHERE grade < '.getAuth('grade').'						
				AND `leave`.company_id = '.getGlobalVar('company')->company_id.'	
				AND leave_status = "waiting"	
			UNION ALL
			SELECT permit_id as request_id, permit.employee_id, first_name, last_name, employee_code, created_at, permit_status as request_status, "request" AS request_type, "permit" AS data 
				FROM permit					
				JOIN employees ON employees.employee_id = permit.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'	
				AND permit.company_id = '.getGlobalVar('company')->company_id.'		
				AND permit_status = "waiting"								
			UNION ALL
			SELECT overtime_id as request_id, created_by as employee_id, first_name, last_name, employee_code, created_at, overtime_status as request_status, "request" AS request_type, "overtime" AS data
				FROM overtime
				JOIN employees ON employees.employee_id = overtime.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'	
				AND overtime.company_id = '.getGlobalVar('company')->company_id.'
				AND overtime_status = "waiting"										
			UNION ALL
			SELECT training_id as request_id, created_by as employee_id, first_name, last_name, employee_code, created_at, training_status as request_status, "request" AS request_type, "training" AS data 
				FROM training
				JOIN employees ON employees.employee_id = training.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'
				AND training.company_id = '.getGlobalVar('company')->company_id.'														
				AND training_status = "waiting"	
			UNION ALL	
			SELECT employee_request_change_id as request_id, employee_request_changes.employee_id, first_name, last_name, employee_code, created_at, request_status, request_type, data
				FROM employee_request_changes
				JOIN employees ON employees.employee_id = employee_request_changes.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'	
				AND employee_request_changes.company_id = '.getGlobalVar('company')->company_id.'		
				AND request_status = "waiting"	
			ORDER BY created_at DESC																		
		';
		
		$response = array(
			'total' => $this->db->from('('.$query.') request')->count_all_results()			
		);				
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	public function request_list() {
		$query = '
			SELECT leave_id as request_id, `leave`.employee_id, first_name, last_name, employee_code, created_at, leave_status as request_status, "request" AS request_type, "leave" AS data
				FROM `leave`
				JOIN employees ON employees.employee_id = `leave`.employee_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id
				JOIN level ON level.level_id = employees.level_id								
				WHERE grade < '.getAuth('grade').'						
				AND `leave`.company_id = '.getGlobalVar('company')->company_id.'	
				AND leave_status = "waiting"	
			UNION ALL
			SELECT permit_id as request_id, permit.employee_id, first_name, last_name, employee_code, created_at, permit_status as request_status, "request" AS request_type, "permit" AS data 
				FROM permit					
				JOIN employees ON employees.employee_id = permit.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'	
				AND permit.company_id = '.getGlobalVar('company')->company_id.'		
				AND permit_status = "waiting"								
			UNION ALL
			SELECT overtime_id as request_id, created_by as employee_id, first_name, last_name, employee_code, created_at, overtime_status as request_status, "request" AS request_type, "overtime" AS data
				FROM overtime
				JOIN employees ON employees.employee_id = overtime.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'	
				AND overtime.company_id = '.getGlobalVar('company')->company_id.'
				AND overtime_status = "waiting"										
			UNION ALL
			SELECT training_id as request_id, created_by as employee_id, first_name, last_name, employee_code, created_at, training_status as request_status, "request" AS request_type, "training" AS data 
				FROM training
				JOIN employees ON employees.employee_id = training.created_by
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'
				AND training.company_id = '.getGlobalVar('company')->company_id.'														
				AND training_status = "waiting"	
			UNION ALL	
			SELECT employee_request_change_id as request_id, employee_request_changes.employee_id, first_name, last_name, employee_code, created_at, request_status, request_type, data
				FROM employee_request_changes
				JOIN employees ON employees.employee_id = employee_request_changes.employee_id
				JOIN level ON level.level_id = employees.level_id
				JOIN employee_identities ON employee_identities.employee_id = employees.employee_id						
				WHERE grade < '.getAuth('grade').'	
				AND employee_request_changes.company_id = '.getGlobalVar('company')->company_id.'		
				AND request_status = "waiting"	
			ORDER BY created_at DESC																		
		';
		
		$response = array(
			'total' => $this->db->from('('.$query.') request')->count_all_results(),
			'data' => array()
		);		
		$requests = $this->db->query($query . ' LIMIT 10');
		foreach ($requests->result() as $request) {
			$since = Carbon\Carbon::createFromDate(
				date('Y', strtotime($request->created_at)),
				date('m', strtotime($request->created_at)),
				date('d', strtotime($request->created_at))
			)->diffForHumans();	
			$detail_url = urlRequestConfirmation($request->data, $request->request_id);						
			$response['data'][] = array(
				'first_name' => $request->first_name,
				'last_name' => $request->last_name,
				'employee_code' => $request->employee_code,
				'request_type' => toSelect(lang('_request_type'), $request->request_type),
				'data' => toSelect(lang('_request_data'), $request->data),
				'created_at' => humanDate($request->created_at),
				'since' => $since,
				'detail_url' => base_url($detail_url)
			);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}	

}