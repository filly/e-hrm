<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends MssController {

	public function __construct() {
		parent::__construct();		
	}

	public function index() {
		if ($this->input->is_ajax_request()) {
			$this->load->library('datatables');

			$response = $this->datatables->collection(function(){				
				if ($filter = $this->input->get('filter')) {				
					if ($filter['division_id'] <> '') {
						$this->db->where('a.division_id', $filter['division_id']);
					}
					if ($filter['department_id'] <> '') {
						$this->db->where('a.department_id', $filter['department_id']);
					}
					if (isset($filter['active']) <> 0) {
						$this->db->where_in('active', $filter['active']);	
					}
				}
				$this->db->select('a.*,b.*,c.*,d.*,e.*,f.*,g.*,h.hire_date')							
						 ->join('employee_identities b', 'b.employee_id = a.employee_id')
						 ->join('divisions c', 'c.division_id = a.division_id', 'left')
						 ->join('departments d', 'd.department_id = a.department_id', 'left')
						 ->join('classes e', 'e.class_id = a.class_id', 'left')
						 ->join('positions f', 'f.position_id = a.position_id')
						 ->join('level g', 'g.level_id = a.level_id', 'left')
						 ->join('employee_hire h', 'h.employee_id = a.employee_id')
						 ->where('g.grade < ', getAuth('grade'))
						 ->where('a.company_id', getGlobalVar('company')->company_id)
						 ->from('employees a');
			})
			->editColumn('employee_status', function($data){
				return toSelect(lang('_employee_status'), $data->employee_status);
			})
			->editColumn('active', function($data){
				return toBoolean($data->active);
			})
			->addColumn('', function(){
				return '
					<a href="#" class="text-primary"><span class="fa fa-search fa-fw"></span></a> 
					<a href="#" class="text-warning"><span class="fa fa-file-pdf-o fa-fw"></span></a>
					<a href="#" class="text-danger"><span class="fa fa-reply fa-fw"></span></a>
				';
			})
			->showColumns('employee_code, first_name, division_code, department_code, class_code, position_code, level, employee_status, active')
			->searchableColumns('employee_code, first_name, last_name')
			->render();
			return $this->output->set_content_type('application/json')->set_output($response);
		}
		$this->load->view('employees/index');
	}

}