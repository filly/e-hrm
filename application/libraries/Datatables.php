<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatables {

	protected $CI;	

	protected $query;

	protected $showColumns = array();

	protected $addColumns = array();

	protected $editColumn = array();

	protected $searchableColumns = array();

	public function __construct() {
		$this->CI = &get_instance();
	}

	public function collection($collection) {
		$this->query = $collection;
		return $this;
	}

	public function showColumns($columns) {
		$parse = explode(',', $columns);
		foreach ($parse as $column) {
			$this->showColumns[] = trim($column);
		}
		return $this;
	}

	public function addColumn($name, $value) {
		$this->addColumns[$name] = $value;
		return $this;
	}

	public function editColumn($name, $value) {
		$this->editColumn[$name] = $value;
		return $this;
	}	

	public function searchableColumns($columns) {
		$parse = explode(',', $columns);
		foreach ($parse as $column) {
			$this->searchableColumns[] = trim($column);
		}		
		return $this;
	}

	public function search() {			
		$search = $this->CI->input->post('search');
		if (count($this->searchableColumns) <> 0) {	
			$this->CI->db->group_start();				
			$searchColumns = $this->searchableColumns;
			$this->CI->db->like($searchColumns[0], $search['value'], false);
			unset($searchColumns[0]);
			foreach ($searchColumns as $searchColumn) {
				$this->CI->db->or_like($searchColumn, $search['value'], false);
			}
			$this->CI->db->group_end();
		}
		
	}

	public function orderBy() {
		$orders = $this->CI->input->post('order');
		foreach ($orders as $order) {
			$column = $this->showColumns[$order['column']];
			$this->CI->db->order_by($column, $order['dir']);
		}
	}

	public function render() {				
		$start = $this->CI->input->post('start');
		$length = $this->CI->input->post('length');			
		call_user_func($this->query);
		$recordsTotal = $this->CI->db->count_all_results();
		
		$this->search();
		$this->orderBy();
		call_user_func($this->query);
		$recordsFiltered = $this->CI->db->count_all_results();
				
		call_user_func($this->query);
		$this->search();
		$this->orderBy();
		$this->CI->db->limit($length, $start);		
		$dataSource = $this->CI->db->get();	
		
		$data = array();
		foreach ($dataSource->result() as $key => $row) {			
			$data[$key] = array();
			foreach ($this->showColumns as $column) {				
				if (isset($this->editColumn[$column])) {
					if (is_callable($this->editColumn[$column])) {
						$data[$key][] = $this->editColumn[$column]($row);	
					} else {
						$data[$key][] = $this->editColumn[$column];	
					}
				} else {
					$data[$key][] = $row->$column;
				}				
			}
			foreach ($this->addColumns as $name => $addColumn) {
				if (is_callable($addColumn)) {
					$data[$key][] = $addColumn($row);
				} else {
					$data[$key][] = $addColumn;
				}
			}
		}
		$response = array(
          'draw'                => intval($this->CI->input->post('draw')),
          'recordsTotal'        => $recordsTotal,
          'recordsFiltered'     => $recordsFiltered,
          'data'                => $data
        );
		return json_encode($response);
	}

}