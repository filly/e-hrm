<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

	protected $CI;

	public $key = 'ace7bc55d815c31cc53fa268f5ce10d7';

	public function __construct() {
		$this->CI = &get_instance();
		$this->CI->load->model('auth_model');
	}

	public function signin($username, $password, $as = 'user' ) {
		$auth = $this->CI->auth_model->signin($username, $this->hash($password));
		if ($auth) {
			$this->set(array(
				'username' => $username,
				'first_name' => $auth->first_name,
				'last_name' => $auth->last_name,
				'employee_id' => $auth->employee_id,
				'role' => $auth->role,
				'grade' => $auth->grade,
				'photo' => $auth->photo
			), $as);
			return true;
		} else {
			return false;
		}
	}

	public function signup($username, $password, $data) {
		return $this->CI->auth_model->signup($username, $this->hash($password), $data);
	}

	public function set($auth, $as = null) {
		if ($as) {
			$auth['as'] = $as;
		} else {
			if (!isset($auth['as'])) {
				$auth['as'] = 'user';
			}
		}
		$this->CI->session->set_userdata('auth', $auth);
	}

	public function signout() {
		$this->CI->session->unset_userdata('auth');
		return true;
	}

	public function get($data = null) {
		$auth = $this->CI->session->userdata('auth');
		if ($data) {
			if (isset($auth[$data])) {
				return $auth[$data];
			} else {
				return false;
			}
		} else {
			return $auth;
		}
	}

	public function isSignedIn() {
        if ($this->CI->session->userdata('auth')) {
            return true;
        } else {
            return false;
        }
    }

    public function signedInAs() {
    	return $this->get('as');
    }

	public function hash($str, $key = null) {
		if (!$key) {
			$key = $this->key;
		}
		return sha1($key.sha1($str).$key);
	}

}

function getAuth($data = null) {
	$CI = &get_instance();
	return $CI->auth->get($data);
}

function isSignedIn() {
	$CI = &get_instance();
	return $CI->auth->isSignedIn();
}

function signedInAs() {
	$CI = &get_instance();
	return $CI->auth->signedInAs();
}