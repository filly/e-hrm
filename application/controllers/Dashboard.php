<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AppController {

	public function __construct() {
		parent::__construct();		
	}

	/*public function index() {
		$this->load->model('employees_model');
		$employee_id = getAuth('employee_id');
		$employee = $this->employees_model->findEmployee($employee_id);
		$addresses = $this->employees_model->getEmployeeAddresses($employee_id);
		$phones = $this->employees_model->getEmployeeMobiles($employee_id);
		$mobiles = $this->employees_model->getEmployeeMobiles($employee_id);
		$emails = $this->employees_model->getEmployeeEmails($employee_id);
		$families = $this->employees_model->getEmployeeFamilies($employee_id);		
		$educations = $this->employees_model->getEmployeeEducations($employee_id);
		$experiences = $this->employees_model->getEmployeeExperiences($employee_id);
		$identityCards = $this->employees_model->getEmployeeIdentityCards($employee_id);
		$families = $families->result();
		foreach ($families as $key => $family) {
			$families[$key]->phones = $this->employees_model->getEmployeePhones($employee_id, $family->employee_family_id);
			$families[$key]->mobiles = $this->employees_model->getEmployeeMobiles($employee_id, $family->employee_family_id);
			$families[$key]->emails = $this->employees_model->getEmployeeEmails($employee_id, $family->employee_family_id);
			$families[$key]->addresses = $this->employees_model->getEmployeeAddresses($employee_id, $family->employee_family_id);
			$families[$key]->educations = $this->employees_model->getEmployeeEducations($employee_id, $family->employee_family_id);
		}
	
		$this->load->view('dashboard', array(
			'employee' => $employee,
			'addresses' => $addresses,
			'phones' => $phones,
			'mobiles' => $mobiles,
			'emails' => $emails,
			'families' => $families,
			'educations' => $educations,
			'experiences' => $experiences,
			'identityCards' => $identityCards
		));
	}*/
	public function index() {
		$this->load->model('employees_model');
		$employee_id = getAuth('employee_id');
		$employee = $this->employees_model->findEmployee($employee_id);
		$requests = $this->employees_model->getEmployeeWaitingRequests($employee_id);
		$this->load->view('dashboard', array(
			'employee' => $employee,	
			'requests' => $requests
		));
	}
}