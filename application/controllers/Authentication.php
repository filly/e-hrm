<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function __construct() {
		parent::__construct();		
	}


	public function signin() {
		if (isSignedIn()) {
			$this->redirect->to('dashboard');
		}		
		if (!$company = $this->app_model->findCompanyBySlug($this->uri->segment(1))) {
			show_404();
		}
		$this->global_var->set('company', $company);
		if ($post = $this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', lang('username'), 'required');
			$this->form_validation->set_rules('password', lang('password'), 'required');
			if (!$this->form_validation->run()) {
				$this->redirect->withValidation()->back();
			}			
			if ($this->auth->signin($post['username'], $post['password'])) {				
				$this->redirect->with('successMessage', lang('success_signin'))->intended(getGlobalVar('company')->slug . '/dashboard');
			} else {
				$this->redirect->with('errorMessage', lang('error_signin'))->back();
			}
		}
		$this->load->view('signin');
	}

	public function signout() {
		$this->auth->signout();
		$this->redirect->to();
	}
	
}