<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['signout'] = 'authentication/signout';	
$route['(:any)'] = 'dashboard/index';	
$route['(:any)/signin'] = 'authentication/signin';	
$route['(:any)/dashboard'] = 'dashboard/index';	
$route['(:any)/profile'] = 'ess/profile/index';	
$route['(:any)/profile/updatephoto'] = 'ess/profile/updatephoto';

$route['(:any)/contacts'] = 'ess/contacts/index';
$route['(:any)/contacts/remove/(:num)'] = 'ess/contacts/remove/$2';

$route['(:any)/addresses'] = 'ess/addresses/index';
$route['(:any)/addresses/add'] = 'ess/addresses/add';
$route['(:any)/addresses/edit/(:num)'] = 'ess/addresses/edit/$2';
$route['(:any)/addresses/remove/(:num)'] = 'ess/addresses/remove/$2';

$route['(:any)/educations'] = 'ess/educations/index';
$route['(:any)/educations/add'] = 'ess/educations/add';
$route['(:any)/educations/edit/(:num)'] = 'ess/educations/edit/$2';
$route['(:any)/educations/remove/(:num)'] = 'ess/educations/remove/$2';

$route['(:any)/experiences'] = 'ess/experiences/index';
$route['(:any)/experiences/add'] = 'ess/experiences/add';
$route['(:any)/experiences/edit/(:num)'] = 'ess/experiences/edit/$2';
$route['(:any)/experiences/remove/(:num)'] = 'ess/experiences/remove/$2';

$route['(:any)/documents'] = 'ess/documents/index';
$route['(:any)/documents/add'] = 'ess/documents/add';
$route['(:any)/documents/edit/(:num)'] = 'ess/documents/edit/$2';
$route['(:any)/documents/remove/(:num)'] = 'ess/documents/remove/$2';

$route['(:any)/identity-cards'] = 'ess/identity_cards/index';
$route['(:any)/identity-cards/add'] = 'ess/identity_cards/add';
$route['(:any)/identity-cards/edit/(:num)'] = 'ess/identity_cards/edit/$2';
$route['(:any)/identity-cards/remove/(:num)'] = 'ess/identity_cards/remove/$2';

$route['(:any)/families'] = 'ess/families/index';
$route['(:any)/families/add'] = 'ess/families/add';
$route['(:any)/families/edit/(:num)'] = 'ess/families/edit/$2';
$route['(:any)/families/remove/(:num)'] = 'ess/families/remove/$2';

$route['(:any)/request-changes'] = 'ess/request_changes/index';
$route['(:any)/request-changes/cancel/(:num)'] = 'ess/request_changes/cancel/$2';

$route['(:any)/leave'] = 'ess/leave/index';
$route['(:any)/leave/request'] = 'ess/leave/request';
$route['(:any)/leave/cancel/(:num)'] = 'ess/leave/cancel/$2';
$route['(:any)/leave/detail/(:num)'] = 'ess/leave/detail/$2';
$route['(:any)/leave/confirmation/(:num)'] = 'ess/leave/confirmation/$2';
$route['(:any)/leave/approve/(:num)'] = 'ess/leave/approve/$2';
$route['(:any)/leave/reject/(:num)'] = 'ess/leave/reject/$2';
$route['(:any)/leave/suspend/(:num)'] = 'ess/leave/suspend/$2';

$route['(:any)/overtime'] = 'ess/overtime/index';
$route['(:any)/overtime/request'] = 'ess/overtime/request';
$route['(:any)/overtime/cancel/(:num)'] = 'ess/overtime/cancel/$2';
$route['(:any)/overtime/detail/(:num)'] = 'ess/overtime/detail/$2';
$route['(:any)/overtime/confirmation/(:num)'] = 'ess/overtime/confirmation/$2';
$route['(:any)/overtime/approve/(:num)'] = 'ess/overtime/approve/$2';
$route['(:any)/overtime/reject/(:num)'] = 'ess/overtime/reject/$2';
$route['(:any)/overtime/suspend/(:num)'] = 'ess/overtime/suspend/$2';

$route['(:any)/training'] = 'ess/training/index';
$route['(:any)/training/request'] = 'ess/training/request';
$route['(:any)/training/cancel/(:num)'] = 'ess/training/cancel/$2';
$route['(:any)/training/detail/(:num)'] = 'ess/training/detail/$2';
$route['(:any)/training/confirmation/(:num)'] = 'ess/training/confirmation/$2';
$route['(:any)/training/approve/(:num)'] = 'ess/training/approve/$2';
$route['(:any)/training/reject/(:num)'] = 'ess/training/reject/$2';
$route['(:any)/training/suspend/(:num)'] = 'ess/training/suspend/$2';

$route['(:any)/permit'] = 'ess/permit/index';
$route['(:any)/permit/request'] = 'ess/permit/request';
$route['(:any)/permit/cancel/(:num)'] = 'ess/permit/cancel/$2';
$route['(:any)/permit/detail/(:num)'] = 'ess/permit/detail/$2';
$route['(:any)/permit/confirmation/(:num)'] = 'ess/permit/confirmation/$2';
$route['(:any)/permit/approve/(:num)'] = 'ess/permit/approve/$2';
$route['(:any)/permit/reject/(:num)'] = 'ess/permit/reject/$2';
$route['(:any)/permit/suspend/(:num)'] = 'ess/permit/suspend/$2';

$route['(:any)/request'] = 'ess/request';

$route['(:any)/services/notifications/request'] = 'mss/services/notifications/request';
$route['(:any)/services/notifications/request/list'] = 'mss/services/notifications/request_list';
$route['(:any)/services/notifications/approval'] = 'ess/services/notifications/approval';
$route['(:any)/services/notifications/approval/list'] = 'ess/services/notifications/approval_list';

$route['(:any)/employees'] = 'mss/employees/index';