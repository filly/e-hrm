<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permit_model extends CI_Model {

	public function __construct() {
		parent::__construct();		
	}

	public function findPermit($permit_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$permit = $this->db->where('permit_id', $permit_id)
							 ->get('permit')
							 ->row();
		return $permit;
	}

	public function cancel($permit_id) {
		$cancel = $this->db->where('permit_id', $permit_id)
						   ->update('permit', array(
						   		'permit_status' => 'canceled'
						   	));
		return $cancel;						   
	}

	public function detail($permit_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.employee_id')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   
						   ->where('a.employee_id', getAuth('employee_id'))
						   ->where('permit_id', $permit_id)
						   ->get('permit a')
						   ->row();
		return $detail;						   
	}

	public function confirmation($permit_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.employee_id')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   
						   ->where('grade < ', getAuth('grade'))
						   ->where('permit_id', $permit_id)
						   ->get('permit a')
						   ->row();
		return $detail;						   
	}

	public function request($type, $date, $reason) {
		$request = $this->db->insert('permit', array(
			'permit_type' => $type,
			'permit_date' => date('Y-m-d H:i:s', strtotime($date)),			
			'reason' => $reason,			
			'permit_status' => 'waiting',
			'company_id' => getGlobalVar('company')->company_id,
			'employee_id' => getAuth('employee_id'),
			'viewed' => 'true',
			'created_at' => date('Y-m-d H:i:s')
		));
		return $request;
	}

	public function approve($permit_id) {
		$approve = $this->db->where('permit_id', $permit_id)
				 ->update('permit', array(
				 	'permit_status' => 'approved',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),				 	
				 	'updated_at' => date('Y-m-d H:i:s')	
				 ));
		return $approve;				 
	}

	public function reject($permit_id) {
		$reject = $this->db->where('permit_id', $permit_id)
				 ->update('permit', array(
				 	'permit_status' => 'rejected',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')	
				 ));
		return $reject;					
	}

	public function suspend($permit_id) {
		$suspend = $this->db->where('permit_id', $permit_id)
				 ->update('permit', array(
				 	'permit_status' => 'waiting',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')					 	
				 ));
		return $suspend;					
	}

}