<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function signin($username, $password) {
		$auth = $this->db->where('a.username', $username)
						 ->where('a.password', $password)
						 ->where('a.company_id', getGlobalVar('company')->company_id)
						 ->where('b.active', 'true')
						 ->join('employees b', 'b.employee_id = a.employee_id')
						 ->join('employee_identities c', 'c.employee_id = a.employee_id')
						 ->join('level d', 'd.level_id = b.level_id')
						 ->get('employee_users a')
						 ->row();								 
		return $auth;				 
	}	

}