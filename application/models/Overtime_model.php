<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overtime_model extends CI_Model {

	public function __construct() {
		parent::__construct();		
	}

	public function findOvertime($overtime_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('created_by', $employee_id);
		}
		$overtime = $this->db->where('overtime_id', $overtime_id)
							 ->get('overtime')
							 ->row();
		return $overtime;
	}	

	public function cancel($overtime_id) {
		$cancel = $this->db->where('overtime_id', $overtime_id)
						   ->update('overtime', array(
						   		'overtime_status' => 'canceled'
						   	));
		return $cancel;						   
	}

	public function getEmployees($overtime_id) {
		$employees = $this->db->join('employees b', 'b.employee_id = a.employee_id')
							  ->join('employee_identities c', 'c.employee_id = a.employee_id')
							  ->where('overtime_id', $overtime_id)
							  ->get('overtime_employees a');
		return $employees;							  
	}

	public function detail($overtime_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.created_by')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   						   
						   ->where('created_by', getAuth('employee_id'))
						   ->where('overtime_id', $overtime_id)
						   ->get('overtime a')
						   ->row();
		return $detail;						   
	}

	public function confirmation($overtime_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.created_by')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   						   
						   ->where('grade < ', getAuth('grade'))
						   ->where('overtime_id', $overtime_id)
						   ->get('overtime a')
						   ->row();
		return $detail;						   
	}

	public function approve($overtime_id) {
		$approve = $this->db->where('overtime_id', $overtime_id)
				 ->update('overtime', array(
				 	'overtime_status' => 'approved',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')		
				 ));
		return $approve;				 
	}

	public function reject($overtime_id) {
		$reject = $this->db->where('overtime_id', $overtime_id)
				 ->update('overtime', array(
				 	'overtime_status' => 'rejected',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),	
				 	'updated_at' => date('Y-m-d H:i:s')				 	
				 ));
		return $reject;					
	}

	public function suspend($overtime_id) {
		$suspend = $this->db->where('overtime_id', $overtime_id)
				 ->update('overtime', array(
				 	'overtime_status' => 'waiting',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')					 	
				 ));
		return $suspend;					
	}

	public function request($date, $start, $end, $reason, $description, $employees = null) {
		$request = $this->db->insert('overtime', array(
			'overtime_date' => date('Y-m-d', strtotime($date)),
			'start_overtime' => $start,
			'end_overtime' => $end,
			'reason' => $reason,
			'description' => $description,
			'overtime_status' => 'waiting',
			'company_id' => getGlobalVar('company')->company_id,
			'viewed' => 'true',
			'created_by' => getAuth('employee_id'),
			'created_at' => date('Y-m-d H:i:s')
		));
		$overtime_id = $this->db->insert_id();
		if (!$employees)	{
			$this->db->insert('overtime_employees', array(
				'overtime_id' => $overtime_id,
				'employee_id' => getAuth('employee_id')
			));
		} else {
			if (is_array($employees)) {
				foreach($employees as $employee) {
					$overtime_employees[] = array(
						'overtime_id' => $overtime_id,
						'employee_id' => $employee
					);
				}
				$this->db->insert_batch('overtime_employees', $overtime_employees);
			} else {
				$this->db->insert('overtime_employees', array(
					'overtime_id' => $overtime_id,
					'employee_id' => $employees
				));
			}
		}
		return true;
	}

}