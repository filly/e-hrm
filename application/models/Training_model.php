<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training_model extends CI_Model {

	public function __construct() {

	}

	public function findTraining($training_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('created_by', $employee_id);
		}
		$overtime = $this->db->where('training_id', $training_id)
							 ->get('training')
							 ->row();
		return $overtime;
	}

	public function cancel($training_id) {
		$cancel = $this->db->where('training_id', $training_id)
						   ->update('training', array(
						   		'training_status' => 'canceled'
						   	));
		return $cancel;						   
	}

	public function getEmployees($training_id) {
		$employees = $this->db->join('employees b', 'b.employee_id = a.employee_id')
							  ->join('employee_identities c', 'c.employee_id = a.employee_id')
							  ->where('training_id', $training_id)
							  ->get('training_employees a');
		return $employees;							  
	}

	public function detail($training_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.created_by')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   
						   ->where('created_by', getAuth('employee_id'))
						   ->where('training_id', $training_id)
						   ->get('training a')
						   ->row();
		return $detail;						   
	}

	public function confirmation($training_id) {
		$detail = $this->db->select('a.*, b.employee_code, c.first_name, c.last_name, e.first_name as confirmed_by_first_name, e.last_name as confirmed_by_last_name')
						   ->join('employees b', 'b.employee_id = a.created_by')
						   ->join('employee_identities c', 'c.employee_id = b.employee_id')
						   ->join('level d', 'd.level_id = b.level_id')		
						   ->join('employee_identities e', 'e.employee_id = a.confirmed_by', 'left')						   
						   ->where('grade < ', getAuth('grade'))
						   ->where('training_id', $training_id)
						   ->get('training a')
						   ->row();
		return $detail;						   
	}

	public function request($training, $start, $end, $cost, $place, $trainer, $employees = null) {
		$request = $this->db->insert('training', array(
			'training' => $training,
			'training_start_date' => date('Y-m-d H:i:s', strtotime($start)),
			'training_end_date' => date('Y-m-d H:i:s', strtotime($end)),
			'cost' => $cost,
			'place' => $place,
			'trainer' => $trainer,
			'training_status' => 'waiting',
			'company_id' => getGlobalVar('company')->company_id,
			'viewed' => 'true',
			'created_by' => getAuth('employee_id'),
			'created_at' => date('Y-m-d H:i:s')
		));
		$training_id = $this->db->insert_id();
		if (!$employees)	{
			$this->db->insert('training_employees', array(
				'training_id' => $training_id,
				'employee_id' => getAuth('employee_id')
			));
		} else {
			if (is_array($employees)) {
				foreach($employees as $employee) {
					$training_employees[] = array(
						'training_id' => $training_id,
						'employee_id' => $employee
					);
				}
				$this->db->insert_batch('training_employees', $training_employees);
			} else {
				$this->db->insert('training_employees', array(
					'training_id' => $training_id,
					'employee_id' => $employees
				));
			}
		}
		return true;
	}

	public function approve($training_id) {
		$approve = $this->db->where('training_id', $training_id)
				 ->update('training', array(
				 	'training_status' => 'approved',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')	
				 ));
		return $approve;				 
	}

	public function reject($training_id) {
		$reject = $this->db->where('training_id', $training_id)
				 ->update('training', array(
				 	'training_status' => 'rejected',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')					 	
				 ));
		return $reject;					
	}

	public function suspend($training_id) {
		$suspend = $this->db->where('training_id', $training_id)
				 ->update('training', array(
				 	'training_status' => 'waiting',
				 	'viewed' => 'false',
				 	'confirmed_by' => getAuth('employee_id'),
				 	'confirmed_at' => date('Y-m-d H:i:s'),
				 	'updated_at' => date('Y-m-d H:i:s')					 	
				 ));
		return $suspend;					
	}

}	