<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends CI_Model {

	public function findCompanyBySlug($slug) {
		$company = $this->db->where('slug', $slug)
				 			->get('companies')
				 			->row();		
		return $company;				 			
	}

}