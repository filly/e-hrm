<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees_model extends CI_Model {

	public function __construct() {
		
	}

	public function getEmploeyees($employee_id, $grade = null) {
		if ($grade) {
			$this->db->where('g.grade < ', $grade);
		}
		$employees = $this->db->select('a.*,b.*,c.*,d.*,e.*,f.*,g.*,h.hire_date')							 
							 ->join('employee_identities b', 'b.employee_id = a.employee_id')
							 ->join('divisions c', 'c.division_id = a.division_id', 'left')
							 ->join('departments d', 'd.department_id = a.department_id', 'left')
							 ->join('classes e', 'e.class_id = a.class_id', 'left')
							 ->join('positions f', 'f.position_id = a.position_id')
							 ->join('level g', 'g.level_id = a.level_id', 'left')
							 ->join('employee_hire h', 'h.employee_id = a.employee_id')
							 ->where('a.employee_id', $employee_id)
							 ->get('employees a')
							 ->row();
		return $employees;							 
	}

	public function findEmployee($employee_id, $grade = null) {
		if ($grade) {
			$this->db->where('g.grade < ', $grade);
		}
		$employee = $this->db->select('a.*,b.*,c.*,d.*,e.*,f.*,g.*,h.hire_date')							
							 ->join('employee_identities b', 'b.employee_id = a.employee_id')
							 ->join('divisions c', 'c.division_id = a.division_id', 'left')
							 ->join('departments d', 'd.department_id = a.department_id', 'left')
							 ->join('classes e', 'e.class_id = a.class_id', 'left')
							 ->join('positions f', 'f.position_id = a.position_id')
							 ->join('level g', 'g.level_id = a.level_id', 'left')
							 ->join('employee_hire h', 'h.employee_id = a.employee_id')
							 ->where('a.employee_id', $employee_id)
							 ->get('employees a')							 
							 ->row();
		return $employee;							 
	}
	

	public function findEmployeeIdentity($employee_id) {
		$identity = $this->db->where('employee_id', $employee_id)
						 	 ->get('employee_identities')
						 	 ->row();
		return $identity;						
	}	

	public function updateEmployeePhoto($photo, $employee_id) {
		$update = $this->db->where('employee_id', $employee_id)
		->update('employee_identities', array(
	   		'photo' => $photo
	 	));
		return $update;				 		 
	}

	public function getEmployeeAddresses($employee_id, $employee_family_id = null) {
		$addresses =  $this->db->where('employee_id', $employee_id)
						       ->where('employee_family_id', $employee_family_id)
							   ->get('employee_addresses');
		return $addresses;							  
	}

	public function findEmployeeAddress($employee_address_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$address = $this->db->where('employee_address_id', $employee_address_id)							
							->get('employee_addresses')
							->row();
		return $address;							
	}

	public function findEmployeeContact($employee_contact_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$contacts = $this->db->where('employee_id', $employee_id)
							->where('employee_contact_id', $employee_contact_id)
							->get('employee_contacts')
							->row();
		return $contacts;							
	}

	public function getEmployeeContacts($employee_id, $employee_family_id = null) {
		$contacts = $this->db->where('employee_id', $employee_id)
							->where('employee_family_id', $employee_family_id)
							->get('employee_contacts');
		return $contacts;							
	}


	public function getEmployeePhones($employee_id, $employee_family_id = null) {
		$mobiles =$this->db->where('media', 'phone')
						   ->where('employee_id', $employee_id)
						   ->where('employee_family_id', $employee_family_id)
						   ->get('employee_contacts');
		return $mobiles;						   
	}

	public function getEmployeeMobiles($employee_id, $employee_family_id = null) {
		$mobiles =$this->db->where('media', 'mobile')
						   ->where('employee_id', $employee_id)
						   ->where('employee_family_id', $employee_family_id)
						   ->get('employee_contacts');
		return $mobiles;						   
	}

	public function getEmployeeEmails($employee_id, $employee_family_id = null) {
		$emails =$this->db->where('media', 'email')
						  ->where('employee_id', $employee_id)
						  ->where('employee_family_id', $employee_family_id)
						  ->get('employee_contacts');
		return $emails;						   
	}

	public function getEmployeeFamilies($employee_id) {
		$families = $this->db->where('employee_id', $employee_id)							 
							 ->get('employee_families');
		return $families;							 
	}	

	public function getEmployeeEducations($employee_id, $employee_family_id = null) {
		$educations = $this->db->where('employee_id', $employee_id)
							   ->where('employee_family_id', $employee_family_id)
							   ->get('employee_educations');
		return $educations;							   
	}

	public function findEmployeeEducation($employee_education_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$address = $this->db->where('employee_education_id', $employee_education_id)
			->get('employee_educations')
			->row();
		return $address;
	}

	public function getEmployeeExperiences($employee_id) {
		$educations = $this->db->where('employee_id', $employee_id)							   
							   ->get('employee_experiences');
		return $educations;							   
	}

	public function findEmployeeExperience($employee_experience_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$address = $this->db->where('employee_experience_id', $employee_experience_id)
			->get('employee_experiences')
			->row();
		return $address;
	}

	public function getEmployeeIdentityCards($employee_id) {
		$identityCards = $this->db->where('employee_id', $employee_id)
								  ->get('employee_identity_cards');
		return $identityCards;								  
	}

	public function findEmployeeIdentityCard($employee_identity_card_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$identityCard = $this->db->where('employee_identity_card_id', $employee_identity_card_id)
			->get('employee_identity_cards')
			->row();
		return $identityCard;
	}

	public function getEmployeeDocuments($employee_id) {
		$documents = $this->db->where('employee_id', $employee_id)
			->get('employee_documents');
		return $documents;
	}

	public function getEmployeeWaitingRequests($employee_id) {
		$request = $this->db->query('
			SELECT * FROM (
			SELECT employee_request_change_id as request_id, created_at, `request_type`, `data` FROM `employee_request_changes` WHERE request_status = "waiting" AND employee_id = "'.$employee_id.'"
			UNION ALL
			SELECT leave_id as request_id, created_at, "request" AS `request_type`, "leave" AS `data` FROM `leave` WHERE leave_status = "waiting" AND employee_id = "'.$employee_id.'"
			UNION ALL
			SELECT permit_id as request_id, created_at, "request" AS `request_type`, "permit" AS `data` FROM `permit` WHERE permit_status = "waiting" AND employee_id = "'.$employee_id.'"
			UNION ALL
			SELECT overtime_id as request_id, created_at, "request" AS `request_type`, "overtime" AS `data` FROM `overtime` WHERE overtime_status = "waiting" AND created_by = "'.$employee_id.'"
			UNION ALL
			SELECT training_id as request_id,created_at, "request" AS `request_type`, "training" AS `data` FROM `training` WHERE training_status = "waiting" AND created_by = "'.$employee_id.'"
			) request
			ORDER BY created_at DESC
		');
		return $request;
	}

	public function getEmployeeWaitingRequestChanges($employee_id, $data = null) {
		if ($data) {
			$this->db->where('data', $data);
		}
		$requests = $this->db->where('employee_id', $employee_id)
							 ->where('request_status', 'waiting')
							 ->get('employee_request_changes');
		return $requests;							 
	}

	public function findEmployeeRequestChange($employee_request_change_id, $employee_id = null) {
		if ($employee_id) {
			$this->db->where('employee_id', $employee_id);
		}
		$request_changes = $this->db->where('employee_request_change_id', $employee_request_change_id)
									->get('employee_request_changes');
		return $request_changes;									
	}

	public function cancelRequestChanges($employee_request_change_id) {
		$cancel = $this->db->where('employee_request_change_id', $employee_request_change_id)
						   ->update('employee_request_changes', array(
						   		'request_status' => 'canceled'
						   	));
		return $cancel;						   
	}

	public function employeeRequestChanges($data, $reques_type, $request_data) {
		$insert = $this->db->insert('employee_request_changes', array(
			'company_id' =>getGlobalVar('company')->company_id,
			'employee_id' => getAuth('employee_id'),
			'data' => $data,
			'request_type' => $reques_type,
			'request_data' => json_encode($request_data),
			'request_status' => 'waiting',
			'created_at' => date('Y-m-d H:i:s')
		));
		return $insert;
	}	

}