$(function() {	
	var dataTables = $('#dataTables').dataTable({
		serverSide: true,
		ajax : {			
			type : 'post'
		},
		sorting: [[2, 'DESC']],
		columnDefs : [
			{targets : 4, class : "text-center"},
			{targets : 5, orderable : false, class : "text-right"}
		]
	});
	$.each($('[name*="filter"]'), function(key, element) {
		$(element).change(function() {
			dataTables.api().ajax.url('?' + $('[name*=filter]').serialize()).load();
		});
	});
});