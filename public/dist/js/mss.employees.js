$(function() {	
	var dataTables = $('#dataTables').dataTable({
		serverSide: true,
		ajax : {			
			type : 'post'
		},
		columnDefs : [						
			{targets : 9, orderable : false, class : "text-right"}
		]		
	});
	$.each($('[name*="filter"]'), function(key, element) {
		$(element).change(function() {
			dataTables.api().ajax.url('?' + $('[name*=filter]').serialize()).load();
		});
	});
});