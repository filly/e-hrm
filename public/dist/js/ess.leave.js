$(function() {	
	var dataTables = $('#dataTables').dataTable({
		serverSide: true,
		ajax : {			
			type : 'post'
		},
		searching : false,
		columnDefs : [			
			{targets : 5, class : "text-center"},
			{targets : 6, orderable : false, class : "text-right"}
		]		
	});
	$.each($('[name*="filter"]'), function(key, element) {
		$(element).change(function() {
			dataTables.api().ajax.url('?' + $('[name*=filter]').serialize()).load();
		});
	});
});