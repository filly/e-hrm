$(function() {
	setInterval(function() {
		$.each($('[data-notifications]'), function(key, element) {				
			$.ajax({
				url: base_url($(element).data('notifications')),
				success: function(response) {
					$('a span', element).remove();			
					if (response['total'] != 0) {
						$('a > i', element).after('<span class="label label-warning">'+response['total']+'</span>');					
					}										
					$(element).click(function(){
						$.ajax({
							url: base_url($(element).data('notifications') + '/list'),
							success:function(source) {						
								$('a span', element).remove();			
								if (source['total'] != 0) {
									$('a > i', element).after('<span class="label label-warning">'+source['total']+'</span>');					
								}										
								var template = Handlebars.compile($($('.menu', element).data('template')).html());	
								$('.menu', element).html(template(source));									
							}
						})
					});								
				}
			});
		})
	}, 10000)
});