$(function() {	
	var dataTables = $('#dataTables').dataTable({
		serverSide: true,
		ajax : {			
			type : 'post'
		},
		columnDefs : [			
			{targets : 4, class : "text-center"},
			{targets : 5, orderable : false, class : "text-right"}
		]		
	});
	$.each($('[name*="filter"]'), function(key, element) {
		$(element).change(function() {
			dataTables.api().ajax.url('?' + $('[name*=filter]').serialize()).load();
		});
	});
});